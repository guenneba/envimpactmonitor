

function init_clusters(div_id,data1,update_callback) {

  var m_data = data1;
  var g_clusters = [];

  // name liste déroulante / usage , unité / details
  document.getElementById(div_id).innerHTML = `
    <p></p>
    <div style="position:relative;" name="clusters_table_block">
      <input type="button" value="+" name="clusters_add_row" style="position: absolute;bottom: 2px;">
      <table name="clusters_table" style="margin-left:2em;">
        <tr> <th class="transparent"></th> <th>cluster</th> <th>usage</th> <th class="transparent"></th>  <th>détails</th> </tr>
      </table>
    </div>
    <p></p>
  `;

  var clusters_table = document.querySelector('#'+div_id + ' table[name="clusters_table"]');

  var res = {};

  res.sync = function(new_data) {
    if(new_data) {
      m_data = new_data;
      // rebuild the interface from scratch
      clusters_table.innerHTML = '<tr> <th class="transparent"></th> <th>cluster</th> <th>usage</th> <th class="transparent"></th>  <th>détails</th> </tr>';
      g_clusters = [];
      var initial_cluster_list = clone_obj(m_data.clusters);
      for(var k in initial_cluster_list) {
        add_cluster_row(k, initial_cluster_list[k]);
      }
    }
    m_data.clusters = {};

    g_clusters.forEach(function (item) {
      m_data.clusters[item.name.value] = {};
      var el = m_data.clusters[item.name.value];
      var cluster_data = params.clusters[item.name.value];
      if(cluster_data.total_elec) {
        el['percentage'] = item.usage.value;
      } else {
        var val = item.usage.value;
        if(item.unit.value=="hours_nodes")
          el['hours_nodes'] = val;
        else if(item.unit.value=="hours_cpus")
          el['hours_nodes'] = val / cluster_data.cpus_per_node;
        else if(item.unit.value=="hours_cores")
          el['hours_nodes'] = val / cluster_data.cores_per_node;
        else if(item.unit.value=="hours_gpus")
          el['hours_nodes'] = val / cluster_data.gpus_per_node;
      }
    });
    // console.log(m_data.clusters);
  }

  // Function to add a row in the officeway_table
  function add_cluster_row(item,entry)
  {
    var tr_el     = document.createElement('tr');
    var td_name   = document.createElement('td');
    var td_usage  = document.createElement('td');
    var td_details= document.createElement('td');

    var cluster_name_el = cluster_selector_el.cloneNode('deep');
    td_name.appendChild(cluster_name_el);

    var cur_nb_el = document.createElement('input');
    cur_nb_el.type='number';
    cur_nb_el.min='0';
    if(entry.percentage)
      cur_nb_el.value = entry.percentage;
    else
      cur_nb_el.value = entry.hours_nodes;
    td_usage.appendChild(cur_nb_el);

    var unit_selector = document.createElement('select');
    var unit_span = document.createElement('span');
    unit_span.className = "unit";
    unit_span.textContent = "%";

    cluster_name_el.addEventListener('change', function(){
      unit_selector.innerHTML= '';
      var cluster = params.clusters[this.value];
      if(cluster.total_elec) {
        // percentage mode
        cur_nb_el.step='0.1';    
        cur_nb_el.max='99.9';
        cur_nb_el.value = Math.min(99,cur_nb_el.value)
        unit_selector.style.display = "none";
        unit_span.style.display = "initial";
      } else {
        cur_nb_el.step='1';    
        cur_nb_el.max='99999999';

        // search for possible units
        function add_unit(key,name) {
          var opt_el = document.createElement('option');
          opt_el.value = key;
          opt_el.innerHTML = name;
          unit_selector.appendChild(opt_el);
        }
        add_unit("hours_nodes","heures.noeuds");
        if(cluster.cpus_per_node)
          add_unit("hours_cpus","heures.cpus");
        if(cluster.cores_per_node)
          add_unit("hours_cores","heures.coeurs");
        if(cluster.gpus_per_node)
          add_unit("hours_gpus","heures.gpus");

        unit_selector.value = 'hours_nodes';
        unit_span.style.display = "none";
        unit_selector.style.display = "initial";
      }

      // todo update details...

      res.sync();
    });
    td_usage.appendChild(unit_selector);
    td_usage.appendChild(unit_span);

    cluster_name_el.value = item;

    unit_selector.addEventListener("change", function() {
      res.sync();
      update_callback();
    });

    cur_nb_el.addEventListener("change", function() {
      res.sync();
      update_callback();
    });
    
    // record the different inputs for future uses/deletion
    g_clusters.push({
      name:  cluster_name_el,
      usage: cur_nb_el,
      unit:  unit_selector,
    });

    tr_el.appendChild(td_sep.cloneNode("deep"));
    tr_el.appendChild(td_name);
    tr_el.appendChild(td_usage);
    tr_el.appendChild(td_sep.cloneNode("deep"));
    tr_el.appendChild(td_details);
    
    clusters_table.appendChild(tr_el);

    trigger_onchange(cluster_name_el); // TODO is it really required?
  }

  // Create a reference "cluster name" selector object to be cloned on demand
  var cluster_selector_el = document.createElement('select');
  for(var k in params.clusters) {
    var cluster = params.clusters[k];
    var opt_el = document.createElement('option');
    opt_el.value = k;
    opt_el.innerHTML = cluster.name;
    cluster_selector_el.appendChild(opt_el);
  }

  var initial_cluster_list = clone_obj(m_data.clusters);
  for(var k in initial_cluster_list) {
    add_cluster_row(k, initial_cluster_list[k]);
  }

  res.sync();

  document.querySelector('#'+div_id + ' input[name="clusters_add_row"]').addEventListener('click',function(){
    add_cluster_row('generic',0);
    var content = document.getElementById(div_id);
    content.style.maxHeight = content.scrollHeight + "px";
    update_callback();
  });

  return res;

}

function process_cluster_list(data) {
  var res = {elec_clusters: 0, grey_CO2: 0};
  for(var k in data) {
    var el = data[k];
    var cluster = params.clusters[k];
    if(cluster.total_elec) {
      res.elec_clusters += el.percentage/100. * cluster.total_elec * cluster.pue;
      res.grey_CO2      += el.percentage/100. * cluster.total_grey_CO2;
    } else {
      res.elec_clusters += el.hours_nodes * cluster.kW_per_node * cluster.pue;
      res.grey_CO2      += el.hours_nodes * cluster.grey_CO2_per_node / (24*365*5) / (cluster.occupancy/100.);
    }
  }
  return res;
}
