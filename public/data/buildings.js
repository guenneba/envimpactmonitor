var buildings = {

  inria_bso_main: {
    name: "Inria Bordeaux - principal",

    elec: 1540000 - 280000*1.7, // moyenne sur les dernières années - plafrim, PUE de 1.7 au pif

    // nombre de place théorique (ex. 2 par bureau, 12 par open-space, etc.)
    seats: 9*(7*2+12) + (3*2+2*6+3) /*R5*/ + (6+6+3) /*R1*/ + (2+4) /*R2*/ + (2*7+3+6) /*R4*/, // = 299

    // nombre d'agent réellement dans le batiment
    workers: 200,  // +/- au pif
  },

  inria_crisam_borel: {
    name: "Inria Sophia Antipolis - Borel",
    elec: 142462 /* conso électrique prises blanches + rouges */ + 28479 /* conso cuisine + poste de garde au prorata des occupants (105/566) */ + 32395 /* clim site complet au prorata de la surface (2278/16676) */ + 185426 /* conso des serveurs au prorata des occupants */, // = 388763 (données 2019)
    seats: 193, // données 2020 SG
    workers: 105, // données 2020 SG
  },
};