
g_data['user'] = {
  // the following parameters should be tuned interactively by the user

  building: "inria_bso_main",
  seats: 1,

  to_office: { // km
    car: 20,
    ecar: 0,
    bike: 0,
    ebike: 0,
    bus: 0,
    motorbike: 0,
    train: 0,
    ter: 0,
    tram: 0,
    tgv: 0,
    walk: 0,
  },

  travels: { // km, taken from inria nat 2017, ~3000 agents
    flight:      9000,
    flight_loc:  1000,
    bus:            0,
    tgv:         3000,
  },

  // voir aussi conso kg viande eq. carcasse en france: https://www.liberation.fr/france/2019/04/04/la-consommation-de-viande-a-re-augmente-en-2018-en-france_1719314
  meals: {
    beef:         5,      // 4.66
    chicken:      5,      // 2.4(poulet) + 2.8(charcuterie)
    fish:         2,      // 2.14
    vege:         2,      // 1.6 (oeufs)
    vegan:        0
  },

  devices: { // nb
    desktop:      1,
    laptop:       0,
    screen:       2,
  },

  working_days:   210, // 5 j/sem. * 52.14 sem. - 35 congé - 8 RTT - ~8 féries

  clusters: {
    //plafrim: {percentage: 0},
    jeanzay_cpu: {hours_nodes: 0}
  },

  plafrim_percentage: 0,
  cluster_cpu_hours:  0,  

  home: {
    elec:         3500,   // kWh par personne avec ECS & chauffage électrique (car pour l'instant on a pas le gaz/fioule/bois),
                          // estimation à partir de nombreuses sources, moyenne plutot optimiste
    car:          6500,   // km voiture annuel (source: https://fr.statista.com/statistiques/484345/distance-parcourue-en-moyenne-par-voiture-france/)
                          // essence c'est plutot 9k, diesel plutot 16k -> moyenne 13k
                          // + taux d'occupation moyen de 2 pers. -> 6.5k
  }
};

g_data['user2']= clone_obj(g_data['user']);