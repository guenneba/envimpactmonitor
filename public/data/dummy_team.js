var team_data =

{
  "default": {
    "building": "inria_bso_main"
  },

  "2012":{
      "purchases": {
        "desktop/powerful": 4
      }
  },

  "2013":{
      "purchases": {
        "macbook_pro_15": 1
      }
  },

  "2014":{
      "purchases": {
        "macbook_pro_15": 1,
        "videoprojector": 1
      }
  },

  "2015":{
      "workers": 16,
      "seats": 20,
      "purchases": {
        "laptop": 1,
        "desktop/powerful": 3,
        "screen": 1
      },
      "travels":{
        "flight":     84000,
        "flight_loc":  6200,
        "tgv":        15000
      },
      "clusters": {
        "jeanzay_gpu4": {"hours_nodes": 1000}
      }
  },

  "2016":{
      "workers":15,
      "seats": 20,
      "purchases": {
        "laptop": 1,
        "desktop/powerful": 1,
        "videoprojector": 1,
        "screen": 3,
        "gpu": 1
      },
      "travels":{
        "flight":     96900,
        "flight_loc":  5200,
        "tgv":         8300
      },
      "clusters": {
        "jeanzay_cpu": {"hours_nodes": 1000}
      }
  },

  "2017":{

      "workers": 15.5,
      "seats": 20,
      "purchases": {
        "macbook_pro_13": 1,
        "screen": 8,
      },
      "travels":{
        "flight":     49900,
        "flight_loc":  5800,
        "tgv":         7200,
        "car":         1200
      },
  },
          
  "2018":{
      "workers": 14,
      "seats":   20,
      "purchases": {
        "precision_tower_5xxx": 1,
        "gpu": 1,
        "screen": 2
      },
      "travels":{
        "flight":      63000,
        "flight_loc":   1500,
        "tgv":         21000
      },
      "clusters": {
        "plafrim": {"percentage": 0.5}
      }
  },

  "2019":{
      "workers": 12, 
      "seats":   14,
      "purchases": {
        "precision_tower_5xxx": 1,
        "precision_tower_3xxx": 1,
        "precision_5xxx": 1,
        "gpu": 4,
        "videoprojector": 4
      },
      "travels":{
        "flight":     75000,
        "flight_loc":  4600,
        "tgv":         6400,
        "car":          800
      },
      "clusters": {
        "plafrim":      {"percentage": 2},
        "jeanzay_cpu":  {"hours_nodes": 1000},
        "jeanzay_gpu4": {"hours_nodes": 1000}
      }
  }
}

