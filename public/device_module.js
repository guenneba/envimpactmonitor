
// returns "type/model" unless model is null or "default" in which case we simply return type
function make_device_name(type,model) {
  if(model && model!="" && model!="default")
    return type+"/"+model;
  return type;
}

// if name matches "type/model" return ["type","model"]
// otherwise check whether "name" is a valid device model,
//    if so return ["type","name"]
//    otherwise "name" is a type and return ["name"]
function device_name_to_type_model(name) {
  var type_model = name.split('/');
  if(type_model.length==2 || ((typeof purchase_types !== 'undefined') && type_model[0] in purchase_types))
    return type_model;
  // search within devices
  for(var k in devices) {
    var model = type_model[0];
    var d = devices[k];
    if(d.models && model in d.models)
      return [k,model];
  }

  return type_model;
}

// returns grey CO2 factor from a type/model pair
// - model is optional
function get_device_factor(type,model) {
  var factor = conv.to_CO2[type];
  if(!devices[type])
    console.log("error unknown device type \"" + type + "\"")
  if(!factor)
    factor = devices[type].grey_CO2;
  if(model && model!='default'){
    if(devices[type].models[model].grey_CO2)
      factor = devices[type].models[model].grey_CO2;
    else
      factor = devices[type].models[model];
  }
  return factor;
}

// returns the attribute "attr" from a type/model pair
// - model is optional 
function get_device_attribute(type,model,attr) {
  var res = undefined;
  var device = devices[type];
  if(device) {
    res = device[attr];
    if(model && model!='default' && devices[type].models[model][attr]){
      res = devices[type].models[model][attr];
    }  
  }
  return res;
}

function consolidate_device_item(type,model,item) {
  return {
    nb:       item.nb ? item.nb : item,
    lifetime: item.lifetime ? item.lifetime : get_device_attribute(type,model,'duration'),
    usage:    item.usage    ? item.usage    : get_device_attribute(type,model,'usage')
  };
}

function for_each_device_item(d,f) {
  if(Array.isArray(d)) {
    d.forEach(function(x) {
      f(Object.keys(x)[0],Object.values(x)[0]);
    });
  } else {
    for(var k in d) {
      f(k,d[k]);
    }
  }
}

// converts a raw list of devices (data) as :
//  {
//    grey: { type1_CO2:  kgCO2e1,  type2_CO2:  kgCO2e2, ... },
//    elec: { type1_elec: kWh1,     type2_elec: kWh2,    ... }
//  }
// where data is of the form:
//  {
//    name1: quantity1,                     // number of items for device "name1"
//    name2: {nb:quantity2, <options> },    // or same + options
//    ...
//  }
// or:
//  [
//    {name1: quantity1},
//    {name2: {nb:quantity2, <options> }},
//    ...
//  ]
// Each "name" can be either: "device_type", "device_type/device_model", or simply "device_model" if unambiguous.
// Then the output's "type" are extracted from the actual "device_type".
// Optionaly, they can be renamed by provifing a _opt.type_renamer(x) function.
// This is handy to cluster different device type into a single category.
//
// It is also possible to specify _opt.working_days to adjust usage computation.
//
// The input data's "<options>" can be:
//  - item.lifetime: to overwrite the default lifetime value for this device
//  - item.usage:    to overwrite the default usage value for this device
// usage can be: "yearly_average", any number in ]0,24], or 365
//  Note that _opt.working_days is used only if usage!=yearly_average and usage!=365
// In ecodiag, usage is always "yearly_average"
function process_raw_device_list(data,_opt) {

  var res = {grey:{},elec:{}};

  var opt = {};
  if(_opt)
    opt = _opt;

  for_each_device_item(data, function(item_name,item_data) {

    var type_model = device_name_to_type_model(item_name);
    var type = type_model[0];
    var model = undefined;
    
    if(type_model.length==2)
      model = type_model[1];

    var item = consolidate_device_item(type,model,item_data);

    var output_type = type;
    if(opt.type_renamer) {
      output_type = opt.type_renamer(type);
    }

    if(item.nb>0) {

      var grey_CO2_per_unit = get_device_factor(type,model);
      var grey_CO2 = item.nb * grey_CO2_per_unit / item.lifetime;
      var key = output_type + "_CO2";
      res.grey = keyval_append(res.grey, key, grey_CO2);

      if(item.usage=='yearly_average') {

        var kWh = get_device_attribute(type,model,'yearly_consumption');
        res.elec = keyval_append(res.elec, output_type + "_elec", item.nb * kWh )

      } else {

        var kW = get_device_attribute(type,model,'power_consumption');

        function usage_2_hours(usage) {
          if(usage==365)
            return 365*24;
          else if(opt.working_days)
            return usage * opt.working_days;
          else
            return usage * 210; // TODO make this customizable
        }
        
        if(kW>0) {
          res.elec = keyval_append(res.elec, output_type + "_elec", item.nb * usage_2_hours(+item.usage) * kW )
        } 

      }
    }
  });

  return res;
}

// This generates the dynamic UI calling update_callback anytime a change is made by the user.
function init_devices(div_id,in_data1,in_data2,update_callback,_opt) {

  var g_devices = [];
  var data1;
  var data2;

  var opt = {show_usage:true,show_COP21_percent:true};
  if(_opt)
    opt = _opt;

  // type liste déroulante / variante liste déroulante / quantité / lifetime0 / [auto] lifetime1 / kgCO2
  document.getElementById(div_id).innerHTML = `
    <p></p>
    <div style="position:relative;" name="devices_table_block">
      <input type="button" value="+" name="devices_add_row" style="position: absolute;bottom: 2px;">
      <table name="devices_table" style="margin-left:2em;">
      </table>
    </div>
    `;

  if(opt.show_usage) {
    document.getElementById(div_id).innerHTML += `
    <span class="unit">Usage : nombre d&#39;heures allumé par jour ouvré, 24h signifie toujours allumé sauf les WE et vacance, 365j signifie jamais éteint.</span>
    `;
  }


  if(opt.show_COP21_percent) {
    document.getElementById(div_id).innerHTML += `
      <p class="whatif">
        Économies réalisables par l&#39;augmentation de la durée de vie de mes équipements : <span class="value" name="ph_lifetime_gain"></span> kg.CO2 par an, soit l&#39;équivalent de <span class="value" name="ph_lifetime_COP21"></span> % de la cible COP21.
      </p>
    `;
  } else {
    // document.getElementById(div_id).innerHTML += `
    //   <p class="whatif">
    //     Économies réalisables par l&#39;augmentation de la durée de vie de mes équipements : <span class="value" name="ph_lifetime_gain"></span> kg.CO2 par an.
    //   </p>
    // `;
  }

  if(opt.show_usage) {
    document.getElementById(div_id).innerHTML += `
      <p class="whatif">
        Économies réalisables par l&#39;extinction des appareils : <span class="value" name="ph_device_usage_gain"></span> kg.CO2 par an,
        soit l&#39;équivalent de <span class="value" name="ph_device_usage_COP21"></span> % de la cible COP21.
      </p>
    `;
  }

  var devices_table = document.querySelector('#'+div_id + ' table[name="devices_table"]');

  var res = {};

  res.sync = function(new_data1,new_data2) {
    if(new_data1) {

      data1 = new_data1;
      if(new_data2)
        data2 = new_data2;

      // rebuild the interface from scratch
      if(opt.show_usage) {
        devices_table.innerHTML = `
          <tr> <th class="transparent" colspan="4"></th>                                                      <th colspan="2">Actuel</th>              <th class="transparent"></th> <th colspan="2">Objectif</th>           <th class="transparent"></th> <th class="transparent"></th></tr>
          <tr> <th class="transparent"></th> <th>modèle</th> <th>quantité</th> <th class="transparent"></th>  <th>durée<br>de vie</th> <th>usage</th>  <th class="transparent"></th> <th>durée<br>de vie</th> <th>usage</th> <th class="transparent"></th> <th>kgCO2e<br>/unité</th></tr>
        `;
      } else {
        devices_table.innerHTML = `
          <tr> <th class="transparent" colspan="4"></th>                                                      <th colspan="2">durée de vie</th>  <th class="transparent"></th> <th colspan="2">kgCO2e/an</th> </tr>
          <tr> <th class="transparent"></th> <th>modèle</th> <th>quantité</th> <th class="transparent"></th>  <th>actuel</th>    <th>objectif</th>  <th class="transparent"></th> <th>fabrication</th> <th>usage</th> </tr>
        `;
      }

      g_devices = [];
      var initial_device_list = clone_obj(data1.devices);
      for_each_device_item(initial_device_list, function(k,v) {
        add_device_row(k, v);
      });
    }

    // update automatic lifetime & usage as well as per row details
    g_devices.forEach(function (item) {

      if(item.newlt.disabled) {
        item.newlt.value = toFixed(2*1.5*item.curlt.value, 0)/2;
      }

      if(item.newusage.disabled && opt.show_usage) {
        item.newusage.value = Number(item.curusage.value)<9 ? Number(item.curusage.value) : 9;
      } else if(!opt.show_usage) {
        item.newusage.value = item.curusage.value;
      }
    });

    data1.devices = [];
    data2.devices = [];
    g_devices.forEach(function (item) {
      var type    = item.type.value;
      var model   = item.model.style.display != "none" ? item.model.value : undefined;
      var factor  = get_device_factor(item.type.value,model);
      
      if(!opt.show_usage) {
        item.co2span.textContent = toFixed(Number(item.curnb.value) * factor / Number(item.curlt.value),1);
        item.co2usage.textContent = toFixed(Number(item.curnb.value) * get_device_attribute(type,model,'yearly_consumption') * conv.to_CO2.elec, 1);
      } else {
        item.co2span.textContent = factor;
      }

      var key = make_device_name(item.type.value,item.model.value);

      var o1 = {};
      o1[key] = { nb: +item.curnb.value, usage: item.curusage.value, lifetime: +item.curlt.value};
      data1.devices.push(o1);
      var o2 = {};
      o2[key] = { nb: +item.curnb.value, usage: item.newusage.value, lifetime: +item.newlt.value};
      data2.devices.push(o2);
    });

    var devices1 = process_raw_device_list(data1.devices, {type_renamer: t => 'total'}, g_data['user'].working_days);
    var devices2 = process_raw_device_list(data2.devices, {type_renamer: t => 'total'}, g_data['user2'].working_days);

    var gain_grey_CO2 = devices1.grey.total_CO2 - devices2.grey.total_CO2;
    if(Number.isNaN(gain_grey_CO2))
      gain_grey_CO2 = 0;
    var lifetime_gain_el = document.querySelector('#'+div_id + ' span[name="ph_lifetime_gain"]');
    if(lifetime_gain_el)
        lifetime_gain_el.textContent = toFixed(gain_grey_CO2,0);
    var lifetime_COP21_el = document.querySelector('#'+div_id + ' span[name="ph_lifetime_COP21"]');
    if(lifetime_COP21_el)
      lifetime_COP21_el.textContent = toFixed(100*Math.abs(gain_grey_CO2)/params.constants.COP21_CO2,1);

    if(opt.show_usage) {
      var gain_elec_CO2 = (devices1.elec.total_elec - devices2.elec.total_elec) * conv.to_CO2.elec;
      document.querySelector('#'+div_id + ' span[name="ph_device_usage_gain"]').textContent = toFixed(gain_elec_CO2,0);
      document.querySelector('#'+div_id + ' span[name="ph_device_usage_COP21"]').textContent = toFixed(100*Math.abs(gain_elec_CO2)/params.constants.COP21_CO2,1);
    }
  }

  // Function to add a row in the officeway_table
  function add_device_row(item_name,item_data)
  {
    var type_model = device_name_to_type_model(item_name);
    var type = type_model[0];
    var model = type_model.length==2 ? type_model[1] : undefined;

    var item = consolidate_device_item(type,model,item_data);

    var tr_el         = document.createElement('tr');
    var td_usage0     = document.createElement('td');
    var td_lt1        = document.createElement('td');
    var td_usage1     = document.createElement('td');

    var device_type_el = device_selector_el.cloneNode('deep');
    device_type_el.value = type;

    var cur_nb_el = document.createElement('input');
    cur_nb_el.type='number';
    cur_nb_el.step='1';
    cur_nb_el.min='0';
    cur_nb_el.max='99999';
    cur_nb_el.size='6';
    cur_nb_el.value = item.nb;

    var cur_lt_el = document.createElement('input');
    cur_lt_el.type='number';
    cur_lt_el.step='0.5';
    cur_lt_el.min='1';
    cur_lt_el.max='99';
    cur_lt_el.size='4';
    cur_lt_el.value = item.lifetime;

    var cur_usage_el = usage_selector_el.cloneNode('deep');
    cur_usage_el.value = opt.show_usage ? get_device_attribute(type,model,'usage') : 'yearly_average';
    td_usage0.appendChild(cur_usage_el);

    var new_lt_el = cur_lt_el.cloneNode('deep');
    new_lt_el.disabled = true;
    create_locker(new_lt_el,td_lt1);
    td_lt1.appendChild(new_lt_el);

    var new_usage_el = cur_usage_el.cloneNode('deep');
    new_usage_el.disabled = true;
    create_locker(new_usage_el,td_usage1);
    td_usage1.appendChild(new_usage_el);

    var co2_el = document.createElement('span');
    co2_el.className = 'unit';

    var co2usage_el = document.createElement('span');
    co2usage_el.className = 'unit';

    var device_model_el = document.createElement('select');
    device_type_el.addEventListener('change', function(){

      device_model_el.innerHTML= '<option value="default">default</option>';
      // device_model_el = document.createElement('select');
      if(devices[this.value].models) {
        for(var m in devices[this.value].models) {
          var opt_el = document.createElement('option');
          opt_el.value = m;
          opt_el.innerHTML = tr(m);
          device_model_el.appendChild(opt_el);
        }
        
        if(this.value==type && model) {
          device_model_el.value = model;
        } else if(opt.default_map && this.value in opt.default_map) {
          device_model_el.value = opt.default_map[this.value];
        }
        else
          device_model_el.value = 'default';

        device_model_el.style.display = "initial";
        res.sync();
        update_callback();
      } else {
        device_model_el.style.display = "none";
      }

      if(devices[this.value].usage && opt.show_usage) {
        cur_usage_el.style.display = "initial";
        new_usage_el.style.display = "initial";
        cur_usage_el.value = devices[this.value].usage;
        new_usage_el.value = devices[this.value].usage;
      } else {
        cur_usage_el.style.display = "none";
        new_usage_el.style.display = "none";
      }

      if(this.value==type) {
        cur_lt_el.value = item.lifetime;
      } else {
        cur_lt_el.value = get_device_attribute(this.value,device_model_el.value,'duration');
      }

    });


    device_type_el.addEventListener("change", function() {
      res.sync();
      update_callback();
    });

    device_model_el.addEventListener("change", function() {
      res.sync();
      update_callback();
    });

    cur_nb_el.addEventListener("change", function() {
      res.sync();
      update_callback();
    });

    cur_lt_el.addEventListener("change", function() {
      res.sync();
      update_callback();
    });

    cur_usage_el.addEventListener("change", function() {
      res.sync();
      update_callback();
    });

    new_lt_el.addEventListener("change", function() {
      res.sync();
      update_callback();
    });

    new_usage_el.addEventListener("change", function() {
      res.sync();
      update_callback();
    });
    
    // record the different inputs for future uses/deletion
    g_devices.push({
      type:  device_type_el,
      model: device_model_el,
      curnb: cur_nb_el,
      curlt: cur_lt_el,
      curusage: cur_usage_el,
      newlt: new_lt_el,
      newusage: new_usage_el,
      co2span: co2_el,
      co2usage: co2usage_el
    });

    append_td(tr_el, device_type_el);
    append_td(tr_el, device_model_el);
    append_td(tr_el, cur_nb_el);
    tr_el.appendChild(td_sep.cloneNode("deep"));
    append_td(tr_el, cur_lt_el);
    if(opt.show_usage) {
      tr_el.appendChild(td_usage0);
      tr_el.appendChild(td_sep.cloneNode("deep"));
      tr_el.appendChild(td_lt1);
      tr_el.appendChild(td_usage1);
    } else {
      tr_el.appendChild(td_lt1);
    }
    tr_el.appendChild(td_sep.cloneNode("deep")); // TODO add a delete button within this cell
    append_td(tr_el, co2_el);
    if(!opt.show_usage) {
      append_td(tr_el, co2usage_el);
    }
    devices_table.appendChild(tr_el);

    trigger_onchange(device_type_el);
  }

  // Create a reference "device name" selector object to be cloned on demand
  var device_selector_el = document.createElement('select');
  var actual_device_types = device_types;
  if(opt.device_types)
    actual_device_types = device_types;
  actual_device_types.forEach(function(item) {
    var opt_el = document.createElement('option');
    opt_el.value = item;
    opt_el.innerHTML = tr(item);
    device_selector_el.appendChild(opt_el);
  });

  var usage_selector_el = document.createElement('select');
  usage_selector_el.innerHTML = `
    <option value="365">365j/an</option>
    <option value="24">24h/j</option>
    <option value="9">9h/j</option>
    <option value="8">8h/j</option>
    <option value="7">7h/j</option>
    <option value="yearly_average">moy</option>
  `;

  res.sync(in_data1,in_data2);

  document.querySelector('#'+div_id + ' input[name="devices_add_row"]').addEventListener('click',function(){
    add_device_row('desktop',1);
    var content = document.getElementById(div_id);
    content.style.maxHeight = content.scrollHeight + "px";
  });

  return res;
}

