

function make_collapsible() {
  var coll = document.getElementsByClassName("collapsible");
  var i;

  for (i = 0; i < coll.length; i++) {
    if(!coll[i].has_collapse_listener) {
      coll[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var content = this.nextElementSibling;
        if (content.style.maxHeight){
          content.style.maxHeight = null;
        } else {
          content.style.maxHeight = content.scrollHeight + "px";
        }
      });
    }
    coll[i].has_collapse_listener = true;
  }
}

function open_tab(evt, tabid, a_tabcontent) {
  var tabcontent_class = a_tabcontent;
  if(!tabcontent_class)
    tabcontent_class = "tabcontent";
  var i, tabcontent, tabactive;
  tabcontent = evt.currentTarget.parentNode.parentNode.querySelectorAll('.' + tabcontent_class);
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tabactive = evt.currentTarget.parentNode.querySelector('.tabactive');
  tabactive.className = tabactive.className.replace(" tabactive", "");
  document.getElementById(tabid).style.display = "block";
  evt.currentTarget.className += " tabactive";
}

function trigger_onchange(element) {
  if ("createEvent" in document) {
    var evt = document.createEvent("HTMLEvents");
    evt.initEvent("change", false, true);
    element.dispatchEvent(evt);
  }
  else
      element.fireEvent("onchange");
}

var clone_element_to_placeholder = function(ph_id, el_id) {
  var ph = document.getElementById(ph_id); // placeholder
  var el = document.getElementById(el_id); // element to be shared/cloned
  var cpy = el.cloneNode(true);
  ph.parentNode.replaceChild(cpy,ph);
  el.classList.add(el_id);
  cpy.classList.add(el_id);
};

var td_sep = document.createElement('td');
td_sep.className='transparent';

var g_locker_count = 0;
function create_locker(target_el, parent_el) {
  var locker_el  = document.createElement('input');
  locker_el.type='checkbox';
  locker_el.className = 'locker';
  locker_el.checked = !target_el.disabled;
  locker_el.id = 'locker_'+g_locker_count++;
  locker_el.onchange = function() {
    target_el.disabled = !this.checked;
  };
  var locker_label  = document.createElement('label');
  locker_label.htmlFor = locker_el.id;
  parent_el.appendChild(locker_el);
  parent_el.appendChild(locker_label);
}


function append_td(tr,nested) {
  var td = document.createElement('td');
  if(Array.isArray(nested)) {
    nested.forEach(function(d){ td.appendChild(d); } );
  } else if(nested) {
    td.appendChild(nested);
  }
  tr.appendChild(td);
  return td;
}


function init_num_controller(id,prefix0,val,conv_func,update_callback) {
  var number_el = document.getElementById(id);
  var prefix = function() { if(typeof prefix0 === "function") return prefix0(); else return prefix0; };
  
  number_el.value = prefix()[val];
  var unit_el;
  if(conv_func) {
    unit_el = document.getElementById(id+'_unit');
  }
  number_el.addEventListener("change", function() {
    prefix()[val] = (conv_func ? conv_func(Number(this.value), unit_el.value) : Number(this.value));
    update_callback();
  });
  if(unit_el) {
    unit_el.addEventListener("change", function() {
      number_el.value = Math.round(10*prefix()[val] * params['to_'+unit_el.value][val])/10;
    });
  }

  var ctrl = {};
  ctrl.sync = function(new_prefix) {
    if(! new_prefix)
      new_prefix = prefix0;
    prefix = function() { if(typeof new_prefix === "function") return new_prefix(); else return new_prefix; };
    number_el.value = prefix()[val];
  }

  return ctrl;
}


function add_rows(table, a_P1, P2, update_callback, data) {

  var P1 = function() { if(typeof a_P1 === "function") return a_P1(); else return a_P1; };

  var res = {update_funcs:[]};

  // update displayed values from stored ones.
  res.update = function() {
    res.update_funcs.forEach(function (func){ func(); });
  }

  data.forEach(function (item){
    if(Object.keys(item).length === 0) {
      var td = document.createElement('td');
      td.colspan=7;
      var tr = document.createElement('tr');
      tr.className = "blank_row";
      tr.appendChild(td)
      table.appendChild(tr);
      return;
    }

    var row = document.createElement('tr');
    var td = document.createElement('td');
    td.innerHTML = item.label;
    row.appendChild(td);
    row.appendChild(td_sep.cloneNode('deep'));

    td = document.createElement('td');
    var input = document.createElement('input');
    input.id = table.id+'_input_'+item.group+'_'+item.id;
    input.type = 'number';
    input.min = item.min;
    input.max = item.max;
    input.step = item.step;
    
    var input2 = input.cloneNode('deep');
    input2.id = table.id+'_input2_'+item.group+'_'+item.id;
    var item_id = item.id;
    var unit;
    var convert = (v,u) => v;
    var unit2 = document.createElement('span');
    unit2.innerHTML = item.unit;
    unit2.className = "unit";
    if(Array.isArray(item.unit)) {
      unit = document.createElement('select');
      unit.value = item.unit[0];
      unit2.innerHTML = item.unit[0];
      //unit.id = ''
      item.unit.forEach(function (u){
        var opt = document.createElement('option')
        opt.value = u;
        opt.innerHTML = u;
        unit.appendChild(opt);
      });

      item_id = item_id+'_'+item.unit[0];
      unit.addEventListener("change", function() {
        unit2.innerHTML = this.value;
        input.value   = Math.round(10*P1()[item.group][item_id] * params['to_'+unit.value][item_id])/10;
        input2.value  = Math.round(10*P2  [item.group][item_id] * params['to_'+unit.value][item_id])/10;
      });

      convert = (v,u) => v * params['to_'+item.unit[0]][item.id+'_'+u];

    } else {
      unit = document.createElement('span');
      unit.innerHTML = item.unit;
      unit.className = "unit";
    }

    input2.disabled = true;
    var unlock_el  = document.createElement('input');
    unlock_el.type = 'checkbox';
    unlock_el.id = table.id+'_locker_'+item.group+'_'+item.id;
    unlock_el.className = 'locker';
    unlock_el.onchange = function() {
      if(P2) input2.disabled = !this.checked;
    };

    input.value = P1()[item.group][item_id];

    res.update_funcs.push(function() {
      input.value = P1()[item.group][item_id];
    });

    input.addEventListener("change", function() {
      P1()[item.group][item_id] = convert(Number(this.value),unit.value);
      if(input2.disabled && P2) {
        P2[item.group][item_id] = P1()[item.group][item_id];
        input2.value = P2[item.group][item_id];
      }
      update_callback();
    });

    if(P2) {
      input2.value = P2[item.group][item_id];
      input2.addEventListener("change", function() {
        P2[item.group][item_id] = convert(Number(this.value),unit.value);
        update_callback();
      });
    }

    td.appendChild(input);
    td.appendChild(unit);
    row.appendChild(td);

    if(P2) {
      row.appendChild(td_sep.cloneNode('deep'));
      td = document.createElement('td');
      td.appendChild(unlock_el);
      td.appendChild(input2);
      td.appendChild(unit2);
    }
    row.appendChild(td);

    row.appendChild(td_sep.cloneNode('deep'));

    td = document.createElement('td');
    td.className = 'note';
    if(item.note)
      td.innerHTML = item.note;
    row.appendChild(td);

    table.appendChild(row);

  });

  return res;
}
