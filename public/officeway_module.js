
function init_office_way(div_id,in_data1,in_data2,update_callback) {

  var g_office_ways = [];
  var data1;
  var data2;

  document.getElementById(div_id).innerHTML = `
    <div style="position:relative;" name="office_table_block">
      <input type="button" value="+" name="officeway_add_row" style="position: absolute;bottom: 2px;">
      <table name="officeway_table" style="margin-left:2em;">
      </table>
    </div>
  `;

  var officeway_table = document.querySelector('#'+div_id + ' table[name="officeway_table"]');

  var res = {};
  res.sync = function(new_data1,new_data2) {
    if(new_data1) {
      data1 = new_data1;

      // rebuild the interface from scratch
      officeway_table.innerHTML = `
        <tr><th colspan="4">Actuel</th><th class="transparent"></th><th colspan="3">Objectif</th><td class="transparent"></td></tr>
        <tr><th>aller-retour</th><th>moyen</th><th>nb passagers supp.</th><th>nb/sem</th><th class="transparent"></th>
        <th>aller-retour</th><th>moyen</th><th>nb passagers supp.</th><td class="transparent"></td></tr>
        `;
      g_office_ways = [];
    }
    if(new_data2) {
      data2 = new_data2;
    }

    var i = 0;
    keyval_foreach(data1, function(k,v) {
      if(Array.isArray(new_data2)){
        add_officeway_row(k,v, new_data2[i].key, new_data2[i].val);
      } else {
        add_officeway_row(k,v);
      }
      ++i;
    });

    sync_officeway();
  }
      


  function sync_officeway() {

    keyval_clear(data1);
    if(data2)
      keyval_clear(data2);

    g_office_ways.forEach(function (item) {

      if(item.newkm.disabled && item.newkm.value != item.curkm.value) {
        item.newkm.value = item.curkm.value;
      }

      if(!item.curway.value.includes('car')) {
        item.curpassengers.value = 0;
        item.curpassengers.disabled = true;
      } else {
        item.curpassengers.disabled = false;
      }

      if(!item.newway.value.includes('car')) {
        item.newpassengers.value = 0;
        item.newpassengers.disabled = true;
      } else {
        item.newpassengers.disabled = false;
      }

      // data1 = keyval_append(data1,item.curway.value,Number(item.curkm.value)*Number(item.occperweek.value)/5./(Number(item.curpassengers.value)+1));
      // if(data2)
      //   data2 = keyval_append(data2,item.newway.value,Number(item.newkm.value)*Number(item.occperweek.value)/5./(Number(item.newpassengers.value)+1));

      data1 = keyval_append(data1,item.curway.value,{dist:Number(item.curkm.value), occperweek: Number(item.occperweek.value), passengers:Number(item.curpassengers.value)});
      if(data2)
        data2 = keyval_append(data2,item.newway.value,{dist:Number(item.newkm.value), occperweek: Number(item.occperweek.value), passengers:Number(item.newpassengers.value)});
      
    });
  }

  // Function to add a row in the officeway_table
  function add_officeway_row(item,val,item2,val2)
  {
    var dist = val;
    var occperweek = 5;
    var passengers = 0;
    if(typeof val === 'object') {
      dist = val.dist;
      passengers = val.passengers;
      occperweek = val.occperweek;
    }

    var dist2 = dist;
    var passengers2 = passengers;
    if(val2) {
      if(typeof val2 === 'object') {
        dist2 = val2.dist;
        passengers2 = val2.passengers;
      } else {
        dist2 = val2;
      }
    }

    var tr_el           = document.createElement('tr');
    var td_km           = document.createElement('td');
    var td_way          = document.createElement('td');
    var td_occperweek   = document.createElement('td');
    var td_nbpassengers = document.createElement('td');
    var td_km_new       = document.createElement('td');
    var td_way_new      = document.createElement('td');
    var td_nbpassengers_new= document.createElement('td');

    var cur_officeway_el = officeway_selector_el.cloneNode('deep');
    cur_officeway_el.value = item;
    var new_officeway_el = officeway_selector_el.cloneNode('deep');
    if(item2)
      new_officeway_el.value = item2;
    else if(item=='bike' || dist<=5*2)
      new_officeway_el.value = 'bike';
    else if(dist<=12*2)
      new_officeway_el.value = 'ebike';
    else
      new_officeway_el.value = 'ter';

    td_way.appendChild(cur_officeway_el);
    td_way_new.appendChild(new_officeway_el);

    var cur_km_el = document.createElement('input');
    cur_km_el.type='number';
    cur_km_el.step='1';
    cur_km_el.min='0';
    cur_km_el.max='500';
    cur_km_el.value = dist;
    td_km.appendChild(cur_km_el);
    var km_el = document.createElement('span');
    km_el.innerHTML = "km";
    km_el.className = 'unit';
    td_km.appendChild(km_el);

    var cur_passengers_el = document.createElement('input');
    cur_passengers_el.type='number';
    cur_passengers_el.step='1';
    cur_passengers_el.min='0';
    cur_passengers_el.max='9';
    cur_passengers_el.value = passengers;
    td_nbpassengers.appendChild(cur_passengers_el);

    var new_km_el = cur_km_el.cloneNode('deep');
    new_km_el.disabled = true;
    new_km_el.value = dist2;
    create_locker(new_km_el,td_km_new);
    td_km_new.appendChild(new_km_el);
    td_km_new.appendChild(km_el.cloneNode('deep'));

    var new_passengers_el = cur_passengers_el.cloneNode('deep');
    new_passengers_el.disabled = true;
    new_passengers_el.value = passengers2;
    create_locker(new_passengers_el,td_nbpassengers_new);
    td_nbpassengers_new.appendChild(new_passengers_el);

    var occ_per_week_el = document.createElement('input');
    occ_per_week_el.type='number';
    occ_per_week_el.step='1';
    occ_per_week_el.min='1';
    occ_per_week_el.max='10';
    occ_per_week_el.value = occperweek;
    td_occperweek.appendChild(occ_per_week_el);

    cur_km_el.addEventListener("change", function() {
      sync_officeway();
      update_callback();
    });

    cur_officeway_el.addEventListener("change", function() {
      sync_officeway();
      update_callback();
    });

    new_km_el.addEventListener("change", function() {
      sync_officeway();
      update_callback();
    });

    new_officeway_el.addEventListener("change", function() {
      sync_officeway();
      update_callback();
    });

    cur_passengers_el.addEventListener("change", function() {
      sync_officeway();
      update_callback();
    });

    occ_per_week_el.addEventListener("change", function() {
      sync_officeway();
      update_callback();
    });

    new_passengers_el.addEventListener("change", function() {
      sync_officeway();
      update_callback();
    });
    
    // record the different inputs for future uses/deletion
    g_office_ways.push({
      curway: cur_officeway_el,
      curkm:  cur_km_el,
      curpassengers: cur_passengers_el,
      occperweek: occ_per_week_el,
      newway: new_officeway_el,
      newkm:  new_km_el,
      newpassengers: new_passengers_el,
    });

    tr_el.appendChild(td_km);
    tr_el.appendChild(td_way);
    tr_el.appendChild(td_nbpassengers);
    tr_el.appendChild(td_occperweek);
    tr_el.appendChild(td_sep.cloneNode("deep"));
    tr_el.appendChild(td_km_new);
    tr_el.appendChild(td_way_new);
    tr_el.appendChild(td_nbpassengers_new);
    tr_el.appendChild(td_sep.cloneNode("deep")); // TODO add a delete button within this cell
    officeway_table.appendChild(tr_el);
  }

  // Create a reference "office-way" selector object to be cloned on demand
  var officeway_selector_el = document.createElement('select');
  officeway_types.forEach(function(item) {
    var opt_el = document.createElement('option');
    opt_el.value = item;
    opt_el.innerHTML = tr(item);
    officeway_selector_el.appendChild(opt_el);
  });

  res.sync(in_data1,in_data2);

  document.querySelector('#'+div_id + ' input[name="officeway_add_row"]').addEventListener('click',function(){
    add_officeway_row('car',0);
    var content = document.getElementById(div_id).parentNode;
    content.style.maxHeight = content.scrollHeight + "px";
  });

  return res;
}
