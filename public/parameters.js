


var meal_profiles={
  'fortement carné': { beef: 7,   chicken: 6,   fish: 1, vege: 0, vegan: 0  },
  'carné'          : { beef: 5,   chicken: 5,   fish: 2, vege: 2, vegan: 0  },
  'carné 6x / sem' : { beef: 3,   chicken: 3,   fish: 1, vege: 5, vegan: 2  },
  'carné 3x / sem' : { beef: 1.5, chicken: 1.5, fish: 1, vege: 6, vegan: 4  },
  'carné 1x / sem' : { beef: 0.5, chicken: 0.5, fish: 1, vege: 6, vegan: 6  },
  'végé'           : { beef: 0,   chicken: 0,   fish: 0, vege: 8, vegan: 6  },
  'végan'          : { beef: 0,   chicken: 0,   fish: 0, vege: 0, vegan: 14 }
};


var grey_types      = ['desktop','screen','laptop'];
var travel_types    = ['flight','flight_loc','car','tgv'];
var officeway_types = ['walk','bike','ebike','bus','tram','ter','train','tgv','motorbike','ecar','car'];
var meal_types      = ['beef','chicken','fish','vege','vegan'];

var device_types    = [
  'desktop','screen','laptop','smartphone','pad','printer',
  'videoprojector', 'keyboard', 'mouse', 'ipphone', 'server'
];

var purchase_types    = [
  'desktop','screen','laptop','smartphone','pad','printer',
  'gpu', 'videoprojector', 'server'
];

var g_year = 2019;
var g_value_type = 'CO2';

var g_data = {};

var params = {
    
    viz:{
      normalize_barplot:      false,
      user_detailed_barplot:  false,
      fuse_piechart_comp:     false,
      mosaic_barplot:         false,
    },
    constants:{
      contrail_factor: 0.1,   // km flight to contrail kgCO2e
      COP21_CO2:      2000,   // maximal kg of CO2 per capita, worldwide, to avoid a burning Earth
      toe_cap_fr:     3.665,  // Tonne of oil equivalent per capita in France
      elec_fam_fr:    6000,   // average kWh of electricity per family in France
      
      onlinevideo:    {CO2: 0.15 , kWh: 0.6}, // kWh electricity for 1h of 1080p video = 1GB, source REN du shiftproject
                                              // * 0.25 kg CO2e ( = mix between electricity in France=0.11 and the world=0.5 )
                                              // BUT
                                              // I tried 10mn of thinkerview on youtube 1080p => 100MB (maybe it is more compressible than average video?)
                                              //    same at 144p => 10MB
                                              //    same as audo podcast => 10MB (make sense @ 128 kpbs)
                                              // video clip @1080p 3:20 => 66MB => 200MB for 10mn
                                              //    same @144p => 7 MB => 20MB for 10mn
                                              // Attention aussi a l'inclusion de l'énergie grise du client!
                                              // sa https://www.greenit.fr/2019/07/22/%ef%bb%bfvideo-en-ligne-quels-impacts-environnementaux/
                                              //                         kWh EP ;  network client server
                                              // - smartphone 4G 1GB  -> 0.63   ;  40%      8%    52%
                                              // - laptop xDSL   3GB  -> 0.45   ;   4%     28%    67%
                                              // - TV box+fibre  5GB  -> 4.52   ;   1%     82%    17%
                                              // - Check:
                                              //   4.52 * 82% -> 3.7 kWh =?= 150W * 1h * 2.5 + 1000kWh / 10 years / (365 * 1h) = 0.375 + 0.3 => n'importe quoi!!!!
                                              //
                                              // For NetFlix : https://www.howtogeek.com/338983/how-much-data-does-netflix-use/
                                              //  2-3 GB/hours
                                              // 

      
    },

    

    working_days:   210, // 5 j/sem. * 52.14 sem. - 35 congé - 8 RTT - ~8 féries
    hours_per_day:  9,
    purchase_lifetime: 5,

    

    plafrim: {
      grey_CO2: 47000,  // TODO fine tune grey energy per year, current calculous below.
      elec: 280000,     //  kWh per year
      pue:  1 + 0.6 + 0.1,        // + 60% for the clim + 10% for the network
        // sources :  https://ecoinfo.cnrs.fr/?p=11099
        //            https://tel.archives-ouvertes.fr/tel-01325363/document
        // cela dit, sur les sites des acteurs du domaine, on peut lire des PUE beaucoup plus proche de 1:
        //  https://atos.net/fr/solutions/calcul-haute-performance-hpc/supercalculateurs-bull-sequana-x/gamme-bullsequana-x1000
        //  https://www.cines.fr/wp-content/uploads/2014/02/GAZETTE-CINES-GCG-2015.pdf -> PUE 1.1

      
      // hypothesis:
      //    nb CPU nodes: 167
      //    nb GPUs: 48
      //    grey kg.CO2e / CPU node: 1300
      //    grey kg.CO2e / GPU     :  400
      //    years of use: 5
      // DELL reports 20% for the fabrication and 80% for use in EU for 4 years of use.
      // Source: https://i.dell.com/sites/csdocuments/CorpComm_Docs/en/carbon-footprint-poweredge-c6420.pdf
      // Given that in EU, we have 1kWh <-> 0.5 kg.CO2e, for 280000 kWh this give us 140000 kg.CO2e
      // of total grey energy, i.e., 28000 per year (+ the GPU). We should check the overall percentage of use of
      // the platform to adjust this other estimation.
      // Nonetheless, this shows that 47000 kg.CO2e is not completely off!
      //
      // Francois : "ce que je peux te dire c'est qu'en moyenne en terme de consommation chaque noeud CPU (bi-socket) consomme aux alentours de 250 W .. 
      // chaque GPU consomme en moyenne 235 W .. sachant que certains noeuds ont 4 GPU ..  les noeuds avec GPU ont d'ailleurs souvent des alim de 1200 / 1500 w"

    },

    labo: {
      elec: 1500000 -280000*1.9 - 220*10*0.2*200, // remove plafrim + 200W per working hours per workers for devices
      gaz_nat_kWh: 0,
      heatnet: 0,
      workers: 200,

      // liste des supercalteurs du GENCI: http://www.genci.fr/fr/content/calculateurs-et-centres-de-calcul
      // OCCIGEN, PUE 1.1 sur la partie calcul. : https://www.cines.fr/publications-recentes/
      //  935 kW pour 50544 cores -> 0.0185 kW/cores -> 
      // 4212 CPUs -> 0.222 kW/cpu
      // 2106 nodes -> 0.444 kW/node
      //
      // CURIE: 1.4 https://www.lemagit.fr/actualites/4500247185/OCCIGEN-le-nouveau-supercalculateur-de-la-recherche-francaise-est-deja-sature
      //  2251 kW
      //
      // JEAN-ZAY
      //  https://www.usinenouvelle.com/editorial/quatre-questions-sur-jean-zay-le-nouveau-supercalculateur-du-cnrs-dedie-a-l-ia.N922244
      //  1400 kW calcul+clim, si clim 10% alors 
      //  1272 kW , (1528 nœuds biprocesseurs à 20 cores, 61120 cores) + (261 node [2 CPU + 4 GPU] + 32 node [2 CPU + 8 GPU])
      //            = 1821 nodes bi-CPU @ 20 core + 1300 GPU
      // si 1 GPU = 250-300 W alors 1300 GPU = 325-390 kW -> il reste 914 pour les CPU
      //  -> 0.0125 kW/core ; 0.502 kW/node ; 0.250 kW/CPU (PDP = 150W)
      // clim, 20% à 50% de plus ?? plutot 10% ? https://lejournal.cnrs.fr/billets/comment-calculer-le-prix-du-calcul
      //
      // JOLIOT-CURIE
      //  79488 cores + 56304 phi cores, = 3312 Xeon 8168 @ 24 cores + 828 Xeon Phi 7250
      //  
    },

    // interface for clusters, dynamic table with "add" button, for each row:
    //    name("Jean-Zay 1","Jean-Zay 2","Plafrim",...,"générique")
    //    amount:(possible units: percentage, h.cores, h.cpu, h.nodes)
    //    details: pue=, [watt_per_XXX], , [occupancy], [grey_CO2_per_node]
    clusters: {

      plafrim: {
        name: 'Plafrim',
        total_grey_CO2: 47000,  // TODO fine tune grey energy per year, current calculous below.
        total_elec: 280000,     //  kWh a year, TODO enable per year value
        pue:  1 + 0.6 + 0.1,
      },

      jeanzay_cpu: {
        name: 'Jean-Zay ("scalaire")',
        kW_per_node: 0.5,
        cpus_per_node: 2,
        cores_per_node: 40,
        pue: 1.1,

        occupancy: 80, // %
        grey_CO2_per_node: 1400,
      },

      jeanzay_gpu4: {
        name: 'Jean-Zay ("convergé" @ 4 GPUs)',
        kW_per_node: 0.5+4*0.275,
        gpus_per_node: 4,
        pue: 1.1,

        occupancy: 80, // %
        grey_CO2_per_node: 1400 + 4*250, // TODO find numbers for GPUs
      },

      jeanzay_gpu8: {
        name: 'Jean-Zay ("convergé" @ 8 GPUs)',
        kW_per_node: 0.5+8*0.275,
        gpus_per_node: 8,
        pue: 1.1,

        occupancy: 80, // %
        grey_CO2_per_node: 1400 + 8*250, // TODO find numbers for GPUs
      },

      joliotcurie_cpu: {
        name: 'Joliot-Curie ("fin SKL")',
        kW_per_node: 0.5,
        cpus_per_node: 2,
        cores_per_node: 48,
        pue: 1.1,

        occupancy: 80, // %
        grey_CO2_per_node: 1400,
      },

      joliotcurie_knl: {
        name: 'Joliot-Curie ("manycore KNL")',
        kW_per_node: 0.358,
        cpus_per_node: 1,
        cores_per_node: 68,
        pue: 1.1,

        occupancy: 80, // %
        grey_CO2_per_node: 250, // TODO au pif
      },

      joliotcurie_amd: {
        name: 'Joliot-Curie ("manycore AMD")',
        kW_per_node: 0.580,
        cpus_per_node: 2,
        cores_per_node: 64,
        pue: 1.1,

        occupancy: 80, // %
        grey_CO2_per_node: 1400, // TODO au pif
      },

      joliotcurie_gpu4: {
        name: 'Joliot-Curie ("IA" @ 4 GPUs)',
        kW_per_node: 1580 /* = 0.5+4*0.270 */,
        gpus_per_node: 4,
        pue: 1.1,

        occupancy: 80, // %
        grey_CO2_per_node: 1400 + 4*250, // TODO find numbers for GPUs
      },

      occigen: {
        name: 'OCCIGEN',
        kW_per_node: 0.44,
        cpus_per_node: 2,
        cores_per_node: 12,
        pue: 1.4,

        occupancy: 80, // %
        grey_CO2_per_node: 1400,
      },
      occigen: {
        name: 'OCCIGEN2',
        kW_per_node: 0.4,
        cpus_per_node: 2,
        cores_per_node: 14,
        pue: 1.2,

        occupancy: 80, // %
        grey_CO2_per_node: 1400,
      },

      curie_fat: {
        name: 'Curie ("fat")',
        kW_per_node: 0.884,
        cpus_per_node: 4,
        cores_per_node: 32,
        pue: 1.4,

        occupancy: 80, // %
        grey_CO2_per_node: 1400,
      },

      curie_thin: {
        name: 'Curie ("thin")',
        kW_per_node: 0.442,
        cpus_per_node: 2,
        cores_per_node: 16,
        pue: 1.4,

        occupancy: 80, // %
        grey_CO2_per_node: 1400,
      },

      curie_hybrid: {
        name: 'Curie ("hybrid")',
        kW_per_node: 982 /* = 0.442+2*0.270 */,
        gpus_per_node: 2,
        pue: 1.4,

        occupancy: 80, // %
        grey_CO2_per_node: 1400 + 2*250, // TODO find numbers for GPUs
      },

      generic: {
        name: 'Générique',
        kW_per_node: 500,
        cpus_per_node: 2,
        pue: 1.2,

        occupancy: 80, // %
        grey_CO2_per_node: 1400,
      },

    },


};

