

function add_perso_travels_and_home_energy_rows(table_id,update_callback) {
  add_rows(document.getElementById(table_id),g_data.perso,g_data.perso2,update_callback,
    [
      {group:'travels',     id:'car',         min:0, max:10000, step:500, unit:'km', label:'Voiture :', note:''},
      {group:'travels',     id:'flight_loc',  min:0, max:99999, step:100, unit:'km', label:'Vols "locaux" :', note:'(destinations atteignables en train de manière raisonable)'},
      {group:'travels',     id:'flight',      min:0, max:99999, step:100, unit:'km', label:'Autres vols :', note:''},
      {group:'travels',     id:'tgv',         min:0, max:99999, step:100, unit:'km', label:'Train :', note:'(TGV)'},
      {group:'travels',     id:'bus',         min:0, max:99999, step:100, unit:'km', label:'Bus :', note:''},
      {},
      {group:'home_energy', id:'elec',        min:0, max:50000, step:10, unit:'kWh', label:'Électricité :', note:''},
      {group:'home_energy', id:'gaz_nat',     min:0, max:50000, step:1, unit:['m3','kWh'], label:'Gaz naturel :', note:'(gaz de ville)'},
      {group:'home_energy', id:'gaz_pp',      min:0, max:50000, step:1, unit:['kg','kWh'], label:'Gaz propane/butane :', note:'(une bouteille classique de 30x60cm contient 13kg)'},
      {group:'home_energy', id:'wood',        min:0, max:50000, step:0.1, unit:['kg','stere','kWh'], label:'Bois :', note:'(inclut buches compressées, granulés...)'},
    ]);
}

function init_perso_interface() {

	// add html content
	document.getElementById('perso_ph').innerHTML = `

  <div class="tab">
    <button class="tablinks tabactive" onclick="open_tab(event, 'perso_gen', 'tabcontent2')">Transport &amp; Énergie</button>
    <button class="tablinks " onclick="open_tab(event, 'perso_meals', 'tabcontent2')">Alimentation</button>
  </div>

	<!-- <button class="collapsible">Transport &amp; Énergie</button>
<div class="content"> -->
<div id="perso_gen" class="tabcontent2" style="display:block;">
  <p></p>
  <table id="yearly_consumption_table">
    <tr><th class="transparent"></th><th class="transparent"></th><th>actuel</th><th class="transparent"></th><th>objectif</th><th class="transparent"></th><th class="transparent"></th></tr>
  </table>
  <p>TODO:</p>
  <ul>
    <li>Composition du foyer adulte vs enfant.</li>
    <li>Poids des animaux de compagnie en kg</li>
    <li>Nombres de semaines hors du logement hiver vs été (implique malus vis à vis de la conso énergétique apparente du logement)</li>
    <li>Nombre d&#39;heures NetFlix/VOD/Youtube</li>
    <li>Vêtements</li>
    <li>Équipement électroménager</li>
    <li>etc.</li>
  </ul>
</div>

<!-- <button class="collapsible">Alimentation</button>
<div class="content"> -->
<div id="perso_meals" class="tabcontent2">
  <p>Régime alimentaire : <select name="input_meal_profile"></select>, objectif : <select name="input_new_meal_profile"></select><p>
  <p class="whatif">
    Cet objectif de changement de régime alimentaire conduit à une économie de <span class="value" name="ph_meal_full_gain"></span> kg.CO2 par an, soit <span class="value" name="ph_meal_full_COP21"></span> % de la cible COP21.
  </p>
  <div id="perso_config_meal_ph"></div>
</div>

<!-- <button class="collapsible"></button>
<div class="content">
  
  <div class="warning">
    <p>Le bilan CO2/énergie <i>vie privée</i> est très largement incomplet.</p>
    <p>Il concerne uniquement l&#39;alimentation, la consommation électrique et les déplacements en voiture qui sont inclus à titre indicatif et de comparaison.</p>
    <p>En particulier, sont ingorés des postes tout aussi important (voir plus) comme : les trajets bus/train/avion perso, chauffage/ECS au gaz/fioul/bois, fabrication/recyclage des appareils (électroménager, TV, smartphone, PC, laptops, box, etc.), navigation web/vidéos, ...</p>
  </div>
</div> -->

<!--
<button class="collapsible">Choix politiques</button>
<div class="content">
  <p class="whatif">
    Fermeture des centrales nucléaires, et remplacement éolien/solaire (60% / 40%).
  </p>
  <p class="whatif">
    Remplacement chauffage et ECS au fioul/gaz par électricité.
  </p>
</div>
-->
`;

	var perso_interface = {};

	perso_interface.update = function () {

      g_perso_data = assemble_perso_data(g_year);
      g_perso_data2 = assemble_perso_data(g_year,2);
      
      yearly_perso_BP.update(g_year,g_value_type);
      detail_perso_PC.update();
    };

	function assemble_perso_data(selected_year,suffix) {
      user_name = "user";
      if(suffix)
        user_name += suffix;
      var user_param = g_data[user_name];

      perso_name = "perso";
      if(suffix)
        perso_name += suffix;
      var perso_param = g_data[perso_name];

      
      // automatically replace local flights by train if AUTO.
      var input2_flight_loc = document.getElementById('yearly_consumption_table_input2_travels_flight_loc');
      var input2_train      = document.getElementById('yearly_consumption_table_input2_travels_tgv');
      if(suffix==2  && input2_flight_loc && (input2_flight_loc.disabled)
                    && input2_train      && (input2_train.disabled) )
      {
        input2_train.value      = g_data.perso2.travels['tgv']      = g_data.perso.travels['tgv'] + g_data.perso.travels['flight_loc'];
        input2_flight_loc.value = g_data.perso2.travels['flight_loc'] = 0;
      }


      var num_meals = 0;
      for(var k in user_param.meals) { num_meals += user_param.meals[k]; }

      var res = {

        travels: perso_param.travels,
        home_energy: perso_param.home_energy,
        meals: clone(user_param.meals,2*365/num_meals),
        devices: perso_param.devices,

        others: {
          french_things_clothes_CO2:          stats.default_french_citizen.french_things_clothes_CO2,
          french_things_others_CO2:           stats.default_french_citizen.french_things_others_CO2,
          french_home_build_CO2:              stats.default_french_citizen.french_home_build_CO2,
          french_home_equipment_CO2:          stats.default_french_citizen.french_home_equipment_CO2,
          french_transport_distribution_CO2:  stats.default_french_citizen.french_transport_distribution_CO2,
          french_services_CO2:                stats.default_french_citizen.french_services_CO2
        },

        //home: g_data.user.home,

        // COP21: {COP21:params.constants.COP21_CO2},

        french_citizen: stats.default_french_citizen,
      };

      // res.devices.device_elec = params.hours_per_day * params.working_days * sum_power_consumption(g_data.user.devices);
      // res.home.meals_CO2 = sum_meals('CO2',g_data.user.meals) * (2*365-params.working_days)/params.working_days;

      return res;
    }

	var g_perso_data  = assemble_perso_data(g_year);
	var g_perso_data2 = assemble_perso_data(g_year,2);

	function make_perso_data_for_barplot(selected_year, value_type) {
		var full_data1 = compute_CO2_and_kWh(g_perso_data);
		var full_data2 = compute_CO2_and_kWh(g_perso_data2);
		// stack data
		//console.log(full_data);
		var subgroups = ['travels','to_office','meals','environment','elec_group','home_energy','devices','others'];
		subgroups.reduce_list = [/*'travels',*/'to_office','devices','meals','environment','elec_group','home_energy','others'];
		var r1 = process_group(ungoup(full_data1, subgroups),'actuel')
		var r2 = process_group(ungoup(full_data2, subgroups),'objectif');
		r2.dim = 0.25;
		var res = [r1,r2];
		res.push(full_data1.find(function(e) { return e.group_name=='french_citizen'; }));
		//console.log(res);
		return res;
	}

	var detail_perso_PC   = make_details_pie_chart(z=>g_perso_data,z=>g_perso_data2, 'perso');
	var yearly_perso_BP   = make_yearly_barplot([g_perso_data,g_perso_data2], g_year,g_value_type,'perso',make_perso_data_for_barplot,{dir:'horizontal', width: 600},
	    {click: function(d) {
	      if(travel_types.includes(d.key))
	        detail_perso_PC.update('travels');
	      else
	        detail_perso_PC.update(d.key);
	    }});

	add_perso_travels_and_home_energy_rows('yearly_consumption_table', perso_interface.update);

	perso_interface.update();
	detail_perso_PC.update('travels');

	return perso_interface;
}
