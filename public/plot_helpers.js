

var d3_insert_line_breaks = function(d) {
    var el = d3.select(this);
    if(d && d.data)
    {
      d = d.value>1 ? tr(d.data.type) : "";
    } else if(!d) {
      d = el.text();
    }
    var words = d.split(' nl ');
    el.text('');

    for (var i = 0; i < words.length; i++) {
        var tspan = el.append('tspan').text(words[i]);
        if (i > 0) {
            tspan.attr('dy', 10);
            if(el.attr('x')) {
              tspan.attr('x', Number(el.attr('x')));
            }
            else {

              tspan.attr('x', 0);
            }
        } else if(el.attr('x')) {
            tspan.attr('dy', -3 - (words.length-2)*5);
        }
    }

    // FIXME: is it still useful ?
    if(el.attr('dy')) {
        var dy_value = parseFloat(el.attr('dy'));
        var dy_unit = el.attr('dy').replace((""+dy_value),"");
        if(el.attr('x') && (!Number.isNaN(dy_value)) && (dy_value>0) && words.length>=2) {
          // el.attr('dy',"-"+(dy_value*words.length/2.0)+dy_unit );
        }
    }
};


// https://stackoverflow.com/questions/5560248/programmatically-lighten-or-darken-a-hex-color-or-rgb-and-blend-colors
const pSBC=(p,c0,c1,l)=>{
    let r,g,b,P,f,t,h,i=parseInt,m=Math.round,a=typeof(c1)=="string";
    if(typeof(p)!="number"||p<-1||p>1||typeof(c0)!="string"||(c0[0]!='r'&&c0[0]!='#')||(c1&&!a))return null;
    if(!this.pSBCr)this.pSBCr=(d)=>{
        let n=d.length,x={};
        if(n>9){
            [r,g,b,a]=d=d.split(","),n=d.length;
            if(n<3||n>4)return null;
            x.r=i(r[3]=="a"?r.slice(5):r.slice(4)),x.g=i(g),x.b=i(b),x.a=a?parseFloat(a):-1
        }else{
            if(n==8||n==6||n<4)return null;
            if(n<6)d="#"+d[1]+d[1]+d[2]+d[2]+d[3]+d[3]+(n>4?d[4]+d[4]:"");
            d=i(d.slice(1),16);
            if(n==9||n==5)x.r=d>>24&255,x.g=d>>16&255,x.b=d>>8&255,x.a=m((d&255)/0.255)/1000;
            else x.r=d>>16,x.g=d>>8&255,x.b=d&255,x.a=-1
        }return x};
    h=c0.length>9,h=a?c1.length>9?true:c1=="c"?!h:false:h,f=this.pSBCr(c0),P=p<0,t=c1&&c1!="c"?this.pSBCr(c1):P?{r:0,g:0,b:0,a:-1}:{r:255,g:255,b:255,a:-1},p=P?p*-1:p,P=1-p;
    if(!f||!t)return null;
    if(l)r=m(P*f.r+p*t.r),g=m(P*f.g+p*t.g),b=m(P*f.b+p*t.b);
    else r=m((P*f.r**2+p*t.r**2)**0.5),g=m((P*f.g**2+p*t.g**2)**0.5),b=m((P*f.b**2+p*t.b**2)**0.5);
    a=f.a,t=t.a,f=a>=0||t>=0,a=f?a<0?t:t<0?a:a*P+t*p:0;
    if(h)return"rgb"+(f?"a(":"(")+r+","+g+","+b+(f?","+m(a*1000)/1000:"")+")";
    else return"#"+(4294967296+r*16777216+g*65536+b*256+(f?m(a*255):0)).toString(16).slice(1,f?undefined:-2)
}

function tr(word) {
  r = {
    'car': 'voiture',
    'ecar':'voiture élec.',
    'bus':'bus',
    'train':'train',
    'tram':'tram',
    'flight':'vols',
    'flight_loc':'vols nl locaux',
    'ebike':'e-vélo',
    'bike':'vélo',

    'desktop':'PC fixe',
    'laptop':'laptop',

    'screen':'écran',
    'screen_upto23': 'jusqu\'à 23"',
    'screen_24to31': '24"-31"',
    'screen_32toinf': '32" et plus',

    'printer':'imprimante',
    'laser_lt_40kg': 'laser à poser (<40kg)', 
    'office_40_99kg': 'laser A3 (40-99kg)',
    'office_ge_100kg': 'laser A3 (>100kg)',


    'pad':'tablette',
    'smartphone':'smartphone',
    'printer':'imprimante',
    'ipphone': 'téléphone IP',
    'keyboard': 'clavier',
    'mouse': 'souris',
    'videoprojector': 'vidéo projecteur',
    'projector_portable': 'transportable',
    'projector_room': 'pour salle',
    'projector_large': 'pour amphi',
    'server': 'serveur',
    'computingserver': 'serveur de calcul',

    'desktop_grey':'PC (fabrication)',
    'laptop_grey':'laptop (fabrication)',
    'screen_grey':'écran (fabrication)',
    'desktop_CO2':'PC (fabrication)',
    'laptop_CO2':'laptop (fabrication)',
    'screen_CO2':'écran (fabrication)',
    'pad_CO2':'tablette (fabrication)',
    'smartphone_CO2':'smartphone (fabrication)',
    'printer_CO2':'imprimante (fabrication)',
    'meat':'viande',
    'elec':'électricité',
    'device_elec':'électricité nl (équipements)',
    'office':'domicile nl travail',
    'to_office':'domicile nl travail',
    'food':'repas nl (travail)',
    'meals':'repas nl (travail)',
    'beef':'repas boeuf',
    'chicken':'repas volaille/porc',
    'fish':'repas poisson',
    'vege':'repas végé',
    'vegan':'repas végan' ,
    'home_food':'repas (perso)',
    'meals_CO2':'repas',
    'food_group':'repas',
    'travel':'missions',
    'travels':'missions',
    'grey':'fabrication',
    'grey_CO2':'fabrication',
    'devices':'équipements',
    'home_car':'voiture (perso)',
    'transport_group':'transports',
    'home_elec':'électricité (perso)',
    'home_energy':'énergie nl logement',
    'energy':'énergie nl batiment',
    'gaz_nat_m3':'gaz nat',
    'gaz_nat_kWh':'gaz nat',
    'gaz_pp_kg':'propane/butane',
    'gaz_pp_kWh':'propane/butane',
    'heatnet': 'réseau de \n chaleur',
    'home':'vie nl privée',
    'COP21':'objectif nl COP21',
    'support_CO2': 'support nl autre',
    'env_elec': 'support nl électricité',
    'elec_plafrim': 'électricité nl plafrim',
    'elec_plafrim_nodes': 'électricité nl plafrim nl (nodes)',
    'elec_plafrim_clim': 'électricité nl plafrim nl (clim & network)',
    'elec_clusters': 'électricité nl autres clusters',
    'elec_group': 'électricité',

    'ecodiag_fabric_and_transport': 'fabrication nl et transport nl (actuel vs objectif)',
    'ecodiag_elec': 'consommation nl électrique',
    'ecodiag_desktop_CO2': 'PC',
    'ecodiag_desktop_elec': 'PC',
    'ecodiag_laptop_CO2': 'laptop',
    'ecodiag_laptop_elec': 'laptop',
    'ecodiag_screen_CO2': 'écran',
    'ecodiag_screen_elec': 'écran',
    'ecodiag_other_CO2': 'autre',
    'ecodiag_other_elec': 'autre',
    'ecodiag_avg_PC': 'moyenne fixe (UC)',
    'avg_WS': 'moyenne station de travail',
    'ecodiag_avg_laptop': 'moyenne',

    'french_citizen': 'français nl moyen',
    'french_meals_CO2': 'alimentation',
    'french_things_clothes_CO2': 'vêtements',
    'french_things_devices_CO2': 'informatique électronique',
    'french_things_others_CO2': 'autres nl objets',
    'french_home_energy_CO2': 'logement nl énergie',
    'french_home_build_CO2': 'logement nl construction',
    'french_home_equipment_CO2': 'logement équipement',
    'french_transport_car_CO2': 'voiture',
    'french_transport_flight_CO2': 'avion',
    'french_transport_others_CO2': 'autre nl transport',
    'french_transport_distribution_CO2': 'fret nl distribution',
    'french_services_CO2': 'services nl publics'
  }[word];
  if(!r)
    return word;
  return r;
}

// maps a type in {'flight','flight_loc','train','car','desktop','laptop',' screen','elec', ...} to a color
function type2color(t,opt){

  var meal_color = 'rgb(102,153,0)';
  var to_office = 'rgb(255,204,1)';
  var energy_color = 'rgb(0,102,204)';
  var travel_color = (opt && opt.group_name!=undefined && opt.group_name=="to_office") ? to_office : 'rgb(255,66,14)';
  var contrast_factor = ((!opt) || opt.contrast==undefined) ? 0.5 : opt.contrast;
  var grey_color = 'rgb(128,128,128)';

  t = t.replace("ecodiag_","");

  var color = {
    'travels': travel_color,
    'flight':     pSBC(-0.3 *contrast_factor,travel_color,false,false),
    'flight_loc': pSBC(-0.2 *contrast_factor,travel_color,false,false),
    'car':        pSBC(-0.1 *contrast_factor,travel_color,false,false),
    'ecar':       pSBC(-0.1 *contrast_factor,travel_color,false,false),
    'motorbike':  pSBC(-0.05*contrast_factor,travel_color,false,false),
    'bus':        pSBC( 0*contrast_factor,travel_color,false,false),
    'ter':        pSBC( 0.075*contrast_factor,travel_color,false,false),
    'train':      pSBC( 0.1*contrast_factor,travel_color,false,false),
    'tgv':        pSBC( 0.125*contrast_factor,travel_color,false,false),
    'tram':       pSBC( 0.15*contrast_factor,travel_color,false,false),
    'ebike':      pSBC( 0.2*contrast_factor,travel_color,false,false),
    'bike':       pSBC( 0.3*contrast_factor,travel_color,false,false),
    'walk':       pSBC( 0.4*contrast_factor,travel_color,false,false),

    'home_elec':          pSBC( 0.1*contrast_factor,energy_color,false,false),
    'env_elec':           pSBC(-0.1*contrast_factor,energy_color,false,false),
    'elec_plafrim_clim':  pSBC( 0.15*contrast_factor,energy_color,false,false),
    'elec_plafrim_nodes': pSBC( 0.1*contrast_factor,energy_color,false,false),
    'elec_plafrim':       pSBC( 0.1*contrast_factor,energy_color,false,false),
    'elec_clusters':      pSBC( 0.1*contrast_factor,energy_color,false,false),
    'elec_total':         pSBC( 0.1*contrast_factor,energy_color,false,false),
    'device_elec':        pSBC( 0.05*contrast_factor,energy_color,false,false),
    'elec':               pSBC( 0*contrast_factor,energy_color,false,false),
    'elec_group':         pSBC( 0*contrast_factor,energy_color,false,false),
    'heatnet':            pSBC(-0.15*contrast_factor,energy_color,false,false),
    'gaz_nat_m3':         pSBC(-0.2*contrast_factor,energy_color,false,false),
    'gaz_nat_kWh':        pSBC(-0.2*contrast_factor,energy_color,false,false),
    'gaz_pp_kg':          pSBC(-0.3*contrast_factor,energy_color,false,false),
    'gaz_pp_kWh':         pSBC(-0.3*contrast_factor,energy_color,false,false),
    'wood_kg':            pSBC(-0.4*contrast_factor,energy_color,false,false),

    'clusters': 'rgb(204,102,255)',

    'to_office': 	to_office,
    'home_car': 	'#f7e6ff',

    'grey_CO2': 		"#666666",
    'devices': 			grey_color,
    'printer_CO2': 		pSBC(-0.1*contrast_factor, grey_color,false,false),
    'desktop_CO2': 	    pSBC( 0*contrast_factor, grey_color,false,false),
    'screen_CO2': 		pSBC( 0.1*contrast_factor, grey_color,false,false),
    'screen_grey': 		pSBC( 0.1*contrast_factor, grey_color,false,false),
    'laptop_CO2': 		pSBC( 0.2*contrast_factor, grey_color,false,false),
    'pad_CO2': 			pSBC( 0.3*contrast_factor, grey_color,false,false),
    'smartphone_CO2': 	pSBC( 0.3*contrast_factor, grey_color,false,false),

    'desktop_elec':  pSBC( 0*contrast_factor, energy_color,false,false),
    'screen_elec':       pSBC( 0.1*contrast_factor, energy_color,false,false),
    'laptop_elec':       pSBC( 0.2*contrast_factor, energy_color,false,false),
    'other_elec':        pSBC( 0.3*contrast_factor, energy_color,false,false),

    'COP21': 			'#1c7018',
    'avg/cap': 			'#871a7a',

    'food':             meal_color,
    'home_food': 		pSBC(0.3,meal_color,false,false),
    'meals': 			meal_color,
    'meals_CO2': 		meal_color,

    'beef':pSBC(-0.2*contrast_factor,meal_color,false,false),
    'chicken':pSBC(-0.1*contrast_factor,meal_color,false,false),
    'fish':pSBC(-0*contrast_factor,meal_color,false,false),
    'vege':pSBC(0.1*contrast_factor,meal_color,false,false),
    'vegan':pSBC(0.2*contrast_factor,meal_color,false,false),
    
    'environment': "rgb(204,102,255)",
    'support_CO2': "#267f8c",

    'energy': 		energy_color,
    'home_energy': 	energy_color,

    'french_meals_CO2': meal_color,
    'french_things_clothes_CO2': '#a6a6a6',
    'french_things_devices_CO2': '#666666',
    'french_things_others_CO2': '#a6a6a6',
    'french_home_energy_CO2': energy_color,
    'french_home_build_CO2': '#a6a6a6',
    'french_home_equipment_CO2': '#666666',
    'french_transport_car_CO2': to_office,
    'french_transport_flight_CO2': '#990000',
    'french_transport_others_CO2': '#ff6666',
    'french_transport_distribution_CO2': '#cc0000',
    'french_services_CO2': '#1caa66'
    }
    [t];

  if(!color)
    color = "rgb(128,128,128)";
  if(opt && opt.dim) {
    color = pSBC(opt.dim,color,false,false);
  }
  return color;
}

function get_val(v,value_type) {
  if(v.val)
    return v.val;
  else if(value_type && v[value_type])
    return v[value_type];
  else
    return v;
}

// returns the sum of electricity consumption in kWh
// for 'desktop','laptop','screen'
function sum_power_consumption(yData) {
    var sum = 0;
    grey_types.forEach(function (k) {
      sum += yData[k] * devices[k].power_consumption;
    });
    return sum;
}

function sum_meals(value_type,meals) {
  if(!meals)
    meals = g_data.user.meals;
  var sum = 0;
  var num_meals = 0;
  meal_types.forEach(function (k) {
    num_meals += meals[k];
    sum += meals[k]*  convfactor(k,value_type);// params.constants['meal_'+k][value_type];
  });
  // normalize to match 1 meal per working-day:
  sum = sum / num_meals * g_data.user.working_days;
  return sum;
}

// output layout:
// [{ .type = {'flight','flight_loc','train','car','desktop','laptop','screen','elec'},
//    .CO2  = ..., // kg CO2 equivalent
//    .kWh  = ...  // energy equivalent
//  }, ... ]
// Used by make_all_years_barplot only, but could be usefull to make summaries.
function extract_CO2_and_kWh(yData) {
    var factors = {} ;
    if(!factors.lifetime)  { factors['lifetime'] = function (t) { return 1.0/devices[t].duration; }; }
    if(!factors.travel)    { factors['travel']   = 1; }
    if(!factors.elec)      { factors['elec']     = 1; }
    if(!factors.meals)     { factors['food']     = 1; }

    // console.log(yData);

    // console.log(travel_types);
    yTravels = travel_types.map(function(t) {
            var val = yData.travels[t];
            return {type:   t,
                    CO2:    conv.to_CO2[t]*val*factors.travel,
                    kWh:    conv.to_kWh[t]*val*factors.travel};
    });
    
    yGrey = grey_types.map(function(t) {
            var nb = yData.devices ? yData.devices[t] : yData[t];
            return {type:   t,
                    CO2:    nb*conv.to_CO2[t]*factors.lifetime(t),
                    kWh:    nb*conv.to_kWh[t]*factors.lifetime(t)};
    });

    var elec_total_kWh = params.hours_per_day * params.working_days * sum_power_consumption(yData) * factors.elec;

    yOthers = [    {type:   'elec',
                    CO2: elec_total_kWh * conv.to_CO2.elec,
                    kWh: elec_total_kWh},

                    {type:  'food',
                     CO2: sum_meals('CO2') * factors.food,
                     kWh: sum_meals('kWh') * factors.food}
              ];

    // merge travels, fabrication, and power consumption
    return yTravels.concat(yGrey.concat(yOthers));
}

// barplot of all years
//  value_type = {'CO2','kWh'}
// TODO: not really useful -> remove ?
function make_all_years_barplot(input_data, value_type,mode,callbacks){

    if(!callbacks)
      callbacks = {};
    if(!callbacks.click)
      callbacks.click = d=>{};

    var barplot={},    barplot_dim = {t: 10, r: 1, b: 1, l: 1};
    barplot_dim.w = 500 - barplot_dim.l - barplot_dim.r, 
    barplot_dim.h = 75 - barplot_dim.t - barplot_dim.b;

    // compute total CO2 per year
    function make_data(value_type) {
        data = [];
        if(mode=='institute') {
          for(yid in input_data) {
              yData = input_data[yid];
              ySummary = extract_CO2_and_kWh(yData); // TODO use compute compute_CO2_and_kWh
              sum = d3.sum(ySummary.map(function(v){ return v[value_type]; }));
              data = data.concat(
                  [{year:yid, val:sum}]
              );
          }
        } else if(mode=='labo') {
          for(yid in input_data) {
            if(Number(yid)) {
              yData = input_data[yid];
              ySummary = extract_CO2_and_kWh(yData); // TODO use compute compute_CO2_and_kWh
              sum = d3.sum(ySummary.map(function(v){ return v[value_type]; }));
              data = data.concat(
                  [{year:yid, val:sum}]
              );
            }
          }
        } else {
          for(var yid in input_data) {
            if(Number(yid)) {
              yData = input_data[yid];
              if(yData.travels) { // skip incomplete years
                  ySummary = extract_CO2_and_kWh(yData); // TODO use compute compute_CO2_and_kWh
                  sum = d3.sum(ySummary.map(function(v){ return v[value_type]; }));
                  data = data.concat(
                      [{year:yid, val:sum}]
                  );
              }
            }
          }
        }
        return data;
    }
    data = make_data(value_type);

    var svg = d3.select('#year_selector_'+mode).append("svg")
        .attr("class", "allyear")
        .attr("width", barplot_dim.w + barplot_dim.l + barplot_dim.r)
        .attr("height", barplot_dim.h + barplot_dim.t + barplot_dim.b).append("g")
        .attr("transform", "translate(" + barplot_dim.l + "," + barplot_dim.t + ")");

    var yAxisGroup = svg.append("g").attr("class", "y axis allyear");

    // create initial barplot

    svg.selectAll("bar").data(data)
      .enter().append("rect")
        .attr("class","allyear")
        .attr("y", function(d) { return barplot_dim.h; })
        .attr("height", function(d) { return 0; })
        .on("click",callbacks.click);

    // Add x-axis to the barplot svg.
    var xAxisGroup = svg.append("g").attr("class", "x axis")
                        .attr("transform", "translate(0," + (barplot_dim.h-30) + ")")
                        .attr("pointer-events","none");

    barplot.update = function(value_type){
        data = make_data(value_type);

        var x = d3.scaleBand().range([0, barplot_dim.w]).padding(0.1)
            .domain(data.map(function(d) { return tr(d.year); }));

        var y = d3.scaleLinear().range([barplot_dim.h, 0])
              .domain([0, d3.max(data, function(d) { return d.val; })]);

        xAxisGroup.call(d3.axisBottom(x));
        yAxisGroup.call(d3.axisLeft(y)
          .ticks(5)
          .tickSize(-barplot_dim.w , 0, 0)
          .tickFormat( function(d) { return d } ));

        svg.selectAll("rect").data(data)
            .attr("x", function(d) { return x(d.year); })
            .attr("width", x.bandwidth())
            .transition().duration(500)
            .style("fill", function(d) { if(d.year==g_year) return "#45f"; else return "#abf"; } )
            .attr("y", function(d) { return y(d.val); })
            .attr("height", function(d) { return barplot_dim.h - y(d.val); });
    };

    return barplot;
}


// Compute something like:
// [
//   [group_name:'travels',
//      {group_name:'travels', 'flight': val1, 'flight_loc': val2, ...,                  total:sum_vals},
//      {group_name:'travels', 'flight': val1, 'flight_loc': val2, ...,                  total:sum_vals, dim:val}, // dim and total are optionals
//      ...
//   ],
//   {group_name:'devices',     'desktop': val1, 'screen': val2, ...,                 total:sum_vals},
//   {group_name:'to_office',   'car': val1, 'bus': val2, 'ebike':val3, ...,              total:sum_vals},
//   {group_name:'meals_group', 'meal': val1, 'home_meal': val2,                          total:sum_vals},
//   {group_name:'elec_group',  'device_elec': val1, 'total_elec': val2,                  total:sum_vals},
//   {group_name:'COP21',       'COP21': val1,                                            total:sum_vals},
//   {group_name:'home_group',  'home_car': val1, 'home_meal': val1, 'home_elec': val3,   total:sum_vals},
//   ...
// ]
// with  val* and sum_vals <-> {CO2:xxx, kWh:yyy, notes:"...."}
// This layout is then directly processed by make_yearly_barplot
// Input data in the form:
// {
//    devices:      {desktop: 1, laptop: 0, screen: 2, device_elec: 392},
//    elec_group:   {elec_total: 1111, device_elec: 392},
//    environment:  {env_elec: 1111, support_CO2: 2000, support_kWh: 2000},
//    home:         {elec: 3500, car: 6500, meals_CO2: 1297},
//    meals:        {beef: 77, chicken: 77, fish: 31, vege: 31, vegan: 0},
//    to_office:    {car: 4360, bike: 0, ebike: 0, bus: 0, train: 0},
//    travels:      {flight: 5835, flight_loc: 576, car: 141, train: 741},
// }
function compute_CO2_and_kWh(data,factors,data2)
{
  // console.log("compute_CO2_and_kWh");
  // console.log(data);
  // console.log(data2);
  var factors = {} ;
  if(!factors.devices)  {
    factors.devices = function (t) {
      var res = 1;
      if(params[t] && params[t].duration) {
        res = 1.0/params[t].duration;
      }
      return res;
    };
  }

  res = [];

  // take as input gdata in the form:
  //    {flight: 49600, flight_loc: 4900, car: 1200, train: 6300}
  // and returns:
  //    {group_name:gname, 'flight': val1, 'flight_loc': val2, ..., total:sum_vals}
  function process_group(gname, gdata, factor) {
    var r = {group_name:gname,total:{CO2:0,kWh:0}};
    for(var t in gdata) {
      var val = gdata[t];
      val = ((typeof factor === "function") ? factor(t) : factor ) * val;

      r[t] = { CO2: convfactor(t,'CO2') * val,
               kWh: convfactor(t,'kWh') * val };
      r.total.CO2 += r[t].CO2;
      r.total.kWh += r[t].kWh;
    }
    return r;
  }

  for(var k in data) {
    if(k!='meta') {
      var f = factors[k] ? factors[k] : 1;
      var g = process_group(k,data[k],f);
      if(data2 && data2[k]) {
        var g2 = process_group(k,data2[k],f);
        g2.dim = 0.5;
        var el = [g,g2];
        el.group_name = k;
        res.push(el);
      }
      else
        res = res.concat(g);
    }
  }

  return res;
}

// This function linearize the output of compute_CO2_and_kWh,
// that is, given:
// [
//   {group_name:'travels',     'flight': val1, 'flight_loc': val2,     total:sum_travels},
//   {group_name:'grey',        'desktop': val1, 'screen': val2,    total:sum_grey},
//   {group_name:'to_office',   'car': val1, 'bus': val2, 'ebike':val3, total:sum_to_office},
//   ...
//   keys: ['flight','flight_loc',etc.],
// ]
// and a list of queried subgroups with optinal reduction list,
// e.g. subgroups=[grey,travels], subgroups.reduce_list=[grey] it generates:
// [
//    {type:'grey', CO2:sum_grey.CO2, kWh:sum_grey.kWh },
//    {type:'flight', CO2:val1.CO2, kWh:val2.kWh},
//    {type:'flight_loc', CO2:val2.CO2, kWh:val2.kWh}
// ]
function ungoup(data,subgroups) {
  var res = [];
  for(var g in subgroups) {
    var gname = subgroups[g];
    var gdata = data.find(function(e) { return e.group_name==gname; });
    if(gdata) {
      if(subgroups.reduce_list.includes(gname)) {
        var r = {type:gname, CO2: gdata.total.CO2, kWh: gdata.total.kWh};
        res.push(r);
      } else {
        for(var e in gdata) {
          if(e!='total' && e!='group_name') {
            var r = {type:e, CO2: gdata[e].CO2, kWh: gdata[e].kWh};
            res.push(r);
          }
        }
      }
    }
  }
  return res;
}

function partial_reduce_inplace(data,reduce_list) {

  function reduce_group(group) {
    var res = {group_name:group.group_name,total:group.total};
    res[group.group_name] = group.total;
    if(group.dim)
      res.dim = group.dim;
    return res;
  }

  var res = [];

  for(var g in data) {
    var gdata = data[g];
    
    if(reduce_list.includes(gdata.group_name)) {
      if(Array.isArray(gdata)) {
        var tmp = [];
        gdata.forEach(function (el) { tmp.push(reduce_group(el)); });
        res[g] = tmp;
        res[g].group_name = gdata.group_name;
      } else {
        res[g] = reduce_group[gdata];
      }
    } else {
      res[g] = gdata;
    }
  }
  return res;
}

function process_group(gdata,gname) {
  var r = {group_name:gname,total:{CO2:0,kWh:0}};
  for(var t in gdata) {
    var val = gdata[t];
    r[val.type] = { CO2: val.CO2, kWh: val.kWh };
    r.total.CO2 += val.CO2;
    r.total.kWh += val.kWh;
  }
  return r;
}


function filter_nan(x) {
    if (Number.isNaN(x)) return 0; else return x;
}


// function to handle yearly barplot,
//      selected_year = {2017,2018,...}
//      value_type = {'CO2','kWh'}
//      mode = {'institute','labo','team','user','perso'}
function make_yearly_barplot(input_data, selected_year, value_type, mode, input_data_getter, opt, callbacks){
    var m_mode = (!mode) ? 'team' : mode;
    var m_horiz = (!opt) || (!opt.dir) ? true : (opt.dir=='horizontal');

    if(!callbacks)
      callbacks = {};
    if(!callbacks.click)
      callbacks.click = d=>{};


    var barplot = {},
        barplot_dim = {t: 20, r: 30, b: 30, l: 40};

    barplot_dim.h = 300 - barplot_dim.t - barplot_dim.b;

    var bar_width_attr = "width";
    var bar_length_attr = "height";
    var bar_x_attr = "x";
    var bar_y_attr = "y";
    

    if(m_horiz)
    {
      barplot_dim.r = 60;
      if(!opt)
        opt = {};
      if(!opt.width)  opt.width = 350;
      if(!opt.height) opt.height = 220;
      barplot_dim.l = 100;
      barplot_dim.h = opt.height;
      barplot_dim.t = 5;
      barplot_dim.b = 30;
      bar_width_attr = "height";
      bar_length_attr = "width";
      bar_x_attr = "y";
      bar_y_attr = "x";
    }

    barplot_dim.w = ((!opt) || (!opt.width) ? 500 : opt.width) - barplot_dim.l - barplot_dim.r;

    var y_null_offset = barplot_dim.h;
    var bar_value_range = barplot_dim.h;
    var bar_category_range = barplot_dim.w;
    
    if(m_horiz)
    {
      y_null_offset = 0;
      bar_value_range = barplot_dim.w;
      bar_category_range = barplot_dim.h;
    }

    

    // shall we normalize input data by team members
    function normalize_data() { return m_mode=='team' && params.viz.normalize_barplot; }
    function mosaic_view() { return m_mode=='user' && params.viz.mosaic_barplot; }
    // function detailed_view() { return m_mode=='user' && (params.viz.user_detailed_barplot || params.viz.mosaic_barplot); }

    function default_make_data(selected_year,value_type) { return compute_CO2_and_kWh(input_data); }

    if(!input_data_getter)
    	input_data_getter = default_make_data;

   	function make_data(selected_year,value_type) {
   		return input_data_getter(selected_year,value_type,opt);
   	}

    // prepare data for barplot plotting
    data = make_data(selected_year,value_type);

    // Create svg for barplot
    var svg = d3.select('#yearly_'+mode).append("svg")
        .attr("width",  barplot_dim.w + barplot_dim.l + barplot_dim.r)
        .attr("height", barplot_dim.h + barplot_dim.t + barplot_dim.b).append("g")
        .attr("transform", "translate(" + barplot_dim.l + "," + barplot_dim.t + ")");

    // Add x-axis to the barplot svg.
    var categoryAxisGroup = svg.append("g").attr("class", "x axis");

    var valueAxisGroup = svg.append("g").attr("class", "y axis");

    if(m_horiz) {
      valueAxisGroup.attr("transform", "translate(0," + barplot_dim.h + ")");
    } else {
      categoryAxisGroup.attr("transform", "translate(0," + barplot_dim.h + ")");
    }

    // create initial barplot
    var stack = d3.stack();
    var has_multi_series = false;
    svg
      .selectAll(".category")
      .data(data, d => d.group_name)
      .enter().append("g").attr("class", "category");

    // Create total labels above the rectangles.
    if(!has_multi_series) {
      svg.append("g").attr("class", "total")
        .selectAll('text')
        .data(data).enter().append('text')
          .attr("text-anchor", m_horiz ? "start" : "middle")
          .attr("dominant-baseline", "middle")
          .attr(bar_y_attr, function(d) { return y_null_offset; });
    }

    // Define the div for the tooltip
    var tooltip = d3.select("body").append("div") 
        .attr("class", "tooltip")       
        .style("opacity", 0);
    
    function mouseover(d){
        // TODO
    }
    
    function mouseout(d){
        // TODO
    }

    var cur_upper_bound = 0;

    var upper_bound_list = [1, 10, 20, 30, 40, 50, 80, 100, 150, 200, 300, 400, 500, 800, 1000, 1500, 2000, 3000, 4000, 5000, 8000, 10000, 15000, 20000, 30000, 40000, 50000, 80000, 100000, 150000, 200000, 300000, 400000, 500000];

    var prev_data;
    
    barplot.update = function(selected_year,value_type){
        // prepare data for barplot plotting
        data = make_data(selected_year,value_type);
        
        // function for x-axis mapping
        x = d3.scaleBand().range([0, bar_category_range]).padding(0.1)
            .domain(data.map(function(d) { return tr(d.group_name); }));

        // compute max value
        var max_val = 0;
        data.forEach(function(d){
        	if(Array.isArray(d)) {
        		d.forEach(function(el){
        			max_val = Math.max(el.total[value_type], max_val);
        		});
        	} else {
        		max_val = Math.max(d.total[value_type], max_val);
        	}
        });
        
        var ideal_upper_bound = upper_bound_list.filter(e => e>=max_val)[0];
        var prev_upper_bound = cur_upper_bound;
        if(max_val>cur_upper_bound)
        	cur_upper_bound = ideal_upper_bound;
        else if(max_val*1.6<cur_upper_bound)
        	cur_upper_bound = ideal_upper_bound;


        var y;
        // function for y-axis mapping
        if(mode=='perso' || normalize_data()) {
          y = d3.scaleLinear().range(m_horiz?[0,bar_value_range]:[0,bar_value_range])
              //.domain([0, d3.max(data, function(d) { return d[1]; })]);
              // .domain([0, value_type=='CO2' ? 16000 : 3000]);
              .domain([0, cur_upper_bound]);
        } else if(mode=='user' || normalize_data()) {
          y = d3.scaleLinear().range(m_horiz?[0,bar_value_range]:[bar_value_range, 0])
              //.domain([0, d3.max(data, function(d) { return d[1]; })]);
                // .domain([0, value_type=='CO2' ? ((m_mode=='user' && opt.stacked=='full') ? 8000 : (mosaic_view() ? 1 : 4000) ) : 3000]);
                .domain([0, ((opt.stacked!='full') && mosaic_view()) ? 1 : cur_upper_bound]);
        } else if(mode=='labo') {
          y = d3.scaleLinear().range(m_horiz?[0, bar_value_range]:[bar_value_range, 0])
                // .domain([0, value_type=='CO2' ? (document.getElementById("input_lab_normalize").checked ? 3000 : 600) : 1200]);
                .domain([0, cur_upper_bound]);
        } else if(mode=='institute') {
          y = d3.scaleLinear().range(m_horiz?[0, bar_value_range]:[bar_value_range, 0])
                //.domain([0, value_type=='CO2' ? 10000 : 400000]);
                .domain([0, cur_upper_bound]);
        } else { // team
          y = d3.scaleLinear().range(m_horiz?[0, bar_value_range]:[bar_value_range, 0])
                // .domain([0, value_type=='CO2' ? 30000 : 50000]);
                .domain([0, cur_upper_bound]);
        }

        if(m_horiz) {
          valueAxisGroup.transition().duration(500).call(d3.axisBottom(y)
            .ticks(5)
            .tickSize(-barplot_dim.h , 0, 0)
            .tickFormat( function(d) { return d } ));

          categoryAxisGroup.call(d3.axisLeft(x));
        } else {
          valueAxisGroup.transition().duration(500).call(d3.axisLeft(y)
            .ticks(5)
            .tickSize(-barplot_dim.w , 0, 0)
            .tickFormat( function(d) { return d } ));

          categoryAxisGroup.call(d3.axisBottom(x));
        }

        // used for mosaic view
        var max_width = data.reduce(function (r,el) {
          if(Array.isArray(el)) {
            return el.reduce((r1,el1) => Math.max(r1,el1.total[value_type]), r);
          } else {
            return Math.max(r,el.total[value_type]);
          }

        }, 0);

        function update_bars(data) {

            has_multi_series = false;
            // update data
            var series = svg
                .selectAll(".category")
                .data(data, d => d.group_name)
                .selectAll(".serie")
                .data(function(d) {
                  // if(mode=="user" && !m_horiz && detailed_view()) console.log(d);
                  if(Array.isArray(d)) {
                    has_multi_series = true;
                    var subdata = stack.keys(Object.keys(d[0]).filter(e => (e!="total" && e!="dim" && e!="group_name" && e!="nb_sub_series")))
                                       .value(function (d, key) { return (!d[key]) ? 0 : get_val(d[key],value_type); })
                                       .offset(mosaic_view() ? d3.stackOffsetExpand : d3.stackOffsetNone)(d);
                    for(var j in d) d[j].nb_sub_series = d.length;
                    return subdata;
                  } else {
                    return stack.keys(Object.keys(d).filter(e => (e!="total" && e!="dim" && e!="group_name")))
                                .value(function (d, key) { return (!d[key]) ? 0 : get_val(d[key],value_type); })([d]);
                  }
                },
                function(d,k) { return d.key;});

            series.exit().remove();

            series.enter().append("g")
                .attr("class", "serie")
                .selectAll("rect")
                .data(function(d) { for(var j in d) {d[j].key = d.key; d[j].group_name=d.group_name} return d; })
                .enter().append("rect")
                  .attr(bar_y_attr, function(d) { return y_null_offset; })
                  .attr(bar_length_attr, function(d) { return 0; })
                  .on("mouseover", function(d,e,f) {
                      tooltip .transition()
                              .duration(200)
                              .style("opacity", .9);
                      tooltip .html(tr(d.key).replace("nl"," ") + "<br>"  + Math.round(get_val(d.data[d.key],value_type)));
                      svg.selectAll("rect").transition().duration(500).style('opacity',0.4);
                      d3.select(this).transition().duration(1).style('opacity',1);
                    })          
                  .on("mouseout", function(d) {
                      tooltip.transition().duration(500).style("opacity", 0);
                      svg.selectAll("rect").transition().duration(500).style('opacity',1);
                    })
                  .on("mousemove", function(d) {
                      tooltip .style("left", (d3.event.pageX+2) + "px")   
                              .style("top",  (d3.event.pageY+5) + "px");
                    })
                  .on("click",callbacks.click)
                  ;

            svg .selectAll(".category")
                .selectAll(".serie")
                .selectAll("rect")
                .data(function(d) { for(var j in d) d[j].key = d.key; return d; } )
                .attr(bar_x_attr, function(d,i) {
                    var base_x = x(tr(d.data.group_name));
                    if(d.data.nb_sub_series)
                      base_x += i*x.bandwidth()/d.data.nb_sub_series;
                    return filter_nan(base_x);
                  })
                .transition().duration(500)
                .style("fill",   function(d) { return type2color(d.key, {dim: (mosaic_view() ? 0 : d.data.dim), contrast: (mosaic_view() ? 2 : 1) , group_name:d.data.group_name}); })
                .attr(bar_y_attr, function(d) { return filter_nan(!d[1] ? y_null_offset : y(d[m_horiz ? 0 : 1])); })
                .attr(bar_width_attr, function(d,i) {
                    if(d.data.nb_sub_series) {
                      //console.log(d);
                      if(mosaic_view())
                        return filter_nan(x.bandwidth() * (d.data.total[value_type] / max_width) / d.data.nb_sub_series);
                      else
                        return filter_nan(x.bandwidth()/d.data.nb_sub_series);
                    } else {
                      return filter_nan(x.bandwidth());
                    }
                  })
                .attr(bar_length_attr, function(d) { return filter_nan(!d[1] ? 0 : Math.abs(y(d[0]) - y(d[1]))); })
                ;

            // update totals
            if(!has_multi_series) {
              svg .selectAll("g.total text")
                    .data(data)
                    .text(function(d){ return filter_nan(Math.round(get_val(d.total,value_type))); } )
                    .attr(bar_x_attr, function(d) { return filter_nan(x(tr(d.group_name))+x.bandwidth()/2); })
                    .transition().duration(500)
                    .attr(bar_y_attr, function(d) { return filter_nan(y(get_val(d.total,value_type)) + (m_horiz ? 5 : -8)); });
            }
            svg.selectAll('g.x.axis g text').each(d3_insert_line_breaks);

            prev_data = data;
        }

        if(prev_data && prev_upper_bound!=cur_upper_bound) {
            update_bars(prev_data);
            window.setTimeout(() => update_bars(data),500);
        } else {
            update_bars(data);
        }
        
    }

    barplot.update(selected_year,value_type);

    return barplot;
}


// function to draw detailed pie chart.
//      mode = {'team','user'}
function make_details_pie_chart(input_data0,input_data1,mode){

    var title_height = 40;
    var pC ={},    pieDim ={w:250, h: 250};
    pieDim.r = Math.min(pieDim.w, pieDim.h) / 2;
            
    // create svg for pie chart.
    var piesvg = d3.select('#yearly_'+mode).append("svg")
        .attr("width", pieDim.w).attr("height", pieDim.h+title_height);

    piesvg.append("text")
          .attr("class","title")
          .style("text-anchor", "middle")
          .style("font-size", "1em")
          .style("fill", "#222")
          .style("font-family","Arial, Helvetica, sans-serif")
          .attr("transform", "translate("+pieDim.w/2+","+title_height/2+")")
          .text("title");

    piesvg.append("g")
        .attr("class","fusedpie")
        .attr("transform", "translate("+pieDim.w/2+","+(pieDim.h/2+title_height)+")");

    piesvg.append("g")
        .attr("class","splitpie0")
        .attr("transform", "translate("+pieDim.w/2+","+(pieDim.h/4+title_height)+")");
    piesvg.append("g")
        .attr("class","splitpie1")
        .attr("transform", "translate("+pieDim.w/2+","+(3*pieDim.h/4+title_height)+")");

    
    // create function to draw the arcs of the pie slices.

    var arcLabel = d3.arc().outerRadius(pieDim.r+40).innerRadius(50);

    // Define the div for the tooltip
    var tooltip = d3.select("body").append("div") 
        .attr("class", "tooltip")       
        .style("opacity", 0);

    var m_key = '';
    var fusedMiddleRadius = pieDim.r - 50;
    var splitOuterRadius = pieDim.r/2 - 5;
    //var fusedOuterRadii = [,pieDim.r - 10];
    var defaultFusedRadii = [pieDim.r - 100,pieDim.r - 10]
    // adjust for area matchin
    defaultFusedRadii[1] = Math.sqrt(2*fusedMiddleRadius*fusedMiddleRadius-defaultFusedRadii[0]*defaultFusedRadii[0]);
    var prevRadii;
    var curRadii;

    // data is expected to be either:
    //   [group_name:'travels',
    //      {group_name:'travels', 'flight': val1, 'flight_loc': val2, ...,                  total:sum_vals},
    //      {group_name:'travels', 'flight': val1, 'flight_loc': val2, ...,                  total:sum_vals, dim:val}, // dim and total are optionals
    //      ...
    //   ]
    // or:
    //   {group_name:'devices',     'desktop': val1, 'screen': val2, ...,                 total:sum_vals}
    pC.update = function(key,value_type) {
      if(!value_type) value_type = 'CO2';
      var given_data;

      if(key)
        m_key = key;
      
      var yData;
      var yData2;
      if(m_key) {
        yData = input_data0();
        yData2 = input_data1 ? input_data1() : input_data0();

        var tmp = compute_CO2_and_kWh(yData, {}, yData2);
        given_data = tmp.find(function(e) { return e.group_name==m_key; });

      }
      if(!given_data)
        return;

      var data = given_data;
      if(!Array.isArray(given_data))
        data = [given_data];

      // we need to convert a raw data to something like:
      //
      // [ {type:'flight', val:1034}, {...}, ... ]
      //
      function make_subdata(data,i) {
        var res = [];
        for(var key in data) {
          if(key!='group_name' && key!='total' && key!='dim') {
            var el = data[key];
            var newel = {type:key, val:get_val(el,value_type)};
            if(data.dim) {
              newel['dim'] = data.dim;
            }
            if(data.group_name) {
              newel['group_name'] = data.group_name;
            }
            if(!(i===undefined)) newel['cat'] = i;
            res.push(newel);
          }
        }
        return res;
      }


      var sum0 = get_val(data[0].total,value_type);
      var sum1 = data.length>=2 ? get_val(data[1].total,value_type) : sum0;
      var maxsum = Math.max(sum0,sum1);
      sum0 = Math.sqrt(sum0/maxsum);
      sum1 = Math.sqrt(sum1/maxsum);
      
      if((!params.viz.fuse_piechart_comp) && data.length == 2) {

        // create a function to compute the pie slice percentage
        var pie = d3.pie().sort(null).value(function(d) { return d.val; });

        if(!curRadii) {
          curRadii = [splitOuterRadius,splitOuterRadius];
        }
        prevRadii = curRadii;
        curRadii = [sum0*splitOuterRadius,sum1*splitOuterRadius];

        piesvg.selectAll(".fusedpie").style("opacity", 0);

        for(var i=0;i<2;++i) {

          var datai = make_subdata(data[i],i);

          piesvg.selectAll(".splitpie"+i).style("opacity", 1);

          var tmp = piesvg
              .selectAll(".splitpie"+i)
              .selectAll("path")
              .data( pie(datai), function(d) { /*console.log(d);*/ return d.data.type; });
          tmp.exit().remove();
          tmp.enter().append("path")
              .each(function(d) { this._current = d; }) // -> for arcTween
              .on("mouseover", function(d,e,f) {
                  tooltip .transition()
                          .duration(200)
                          .style("opacity", .9);
                  tooltip .html(tr(d.data.type).replace("nl"," ") + "<br/>"  + Math.round(d.data.val));
                  piesvg.selectAll(".splitpie"+i).selectAll("path").transition().duration(500).style('opacity',0.4);
                  d3.select(this).transition().duration(1).style('opacity',1);
                })
              .on("mouseout", function(d) {
                  tooltip.transition().duration(500).style("opacity", 0);
                  piesvg.selectAll(".splitpie"+i).selectAll("path").transition().duration(500).style('opacity',1);
                })
              .on("mousemove", function(d) {
                  tooltip .style("left", (d3.event.pageX+2) + "px")
                          .style("top", (d3.event.pageY + 5) + "px");
                })
              ;

            piesvg.selectAll(".splitpie"+i).selectAll("path")
                  .style("fill", function(d) { return type2color(d.data.type,{contrast:2,group_name:d.data.group_name}); })
                  .transition().duration(500)
                  .attrTween("d", arcTweenSplit);
        }

      }
      else 
      {
        if(!curRadii) {
          curRadii = defaultFusedRadii;
        }
        prevRadii = curRadii;
        //sum0 = sum1 = 1;
        curRadii = [defaultFusedRadii[0]*Math.sqrt((sum0+sum1)/(2*sum0)),defaultFusedRadii[1]*Math.sqrt((sum0+sum1)/(2*sum0))];
        

        // create a function to compute the pie slice percentage
        var pie = d3.pie().sort(null).value(function(d) { return d.val; });
        var sum = d3.sum(make_subdata(data[0]).map(function(v){ return v[value_type]; })); // FIXME shall we simply reuse raw_data.total ??

        for(var i=0;i<2;++i) {
          piesvg.selectAll(".splitpie"+i).style("opacity", 0);
        }
        piesvg.selectAll(".fusedpie").style("opacity", 1);

        var cat = piesvg.selectAll(".fusedpie").selectAll(".cat")
            .data(data);
        cat.exit().remove();
        cat.enter().append("g").attr("class","cat");

        var tmp = piesvg.selectAll(".fusedpie").selectAll(".cat")
            .selectAll("path")
            .data(function(d,i) { /*console.log(d); sd = make_subdata(d,i); console.log(sd);*/ return pie(make_subdata(d,i)); },
                  function(d) { /*console.log(d);*/ return d.data.type; });
        tmp.exit().remove();
        tmp.enter().append("path")
            .each(function(d) { this._current = d; }) // ??
            .on("mouseover", function(d,e,f) {
                tooltip .transition()
                        .duration(200)
                        .style("opacity", .9);
                tooltip .html(tr(d.data.type).replace("nl"," ") + "<br/>"  + Math.round(d.data.val));
                piesvg.selectAll(".fusedpie").selectAll("path").transition().duration(500).style('opacity',0.4);
                d3.select(this).transition().duration(1).style('opacity',1);
              })
            .on("mouseout", function(d) {
                tooltip.transition().duration(500).style("opacity", 0);
                piesvg.selectAll(".fusedpie").selectAll("path").transition().duration(500).style('opacity',1);
              })
            .on("mousemove", function(d) {
                tooltip .style("left", (d3.event.pageX+2) + "px")
                        .style("top", (d3.event.pageY + 5) + "px");
              })
            ;

          piesvg.selectAll(".fusedpie").selectAll("path")
                .style("fill", function(d) { return type2color(d.data.type,{contrast:2,group_name:d.data.group_name}); })
                .transition().duration(500)
                .attrTween("d", arcTweenFused);

          // var tmp1 = piesvg
          //       .selectAll('text')
          //       .data(pie(data))
          // tmp1.exit().remove();
          // tmp1.enter().append('text')
          //       // TODO move all these style to css
          //       .style("text-anchor", "middle")
          //       .style("font-size", "0.8em")
          //       .style("fill", "#fcfcfc")
          //       .style("font-family","Arial, Helvetica, sans-serif")
          //       .attr("pointer-events","none");

          // piesvg.selectAll('text').data(pie(ySummary))
          //       .text(function(d){ return d.value/sum>0.03 ? tr(d.data.type) : ""})
          //       .transition().duration(500)
          //       .attr("transform", function(d) { return "translate(" + arcLabel.centroid(d) + ")";  });
      }
      piesvg.selectAll('.title').text(tr(m_key));
      piesvg.selectAll('.title').each(d3_insert_line_breaks);
    }

    function mouseover(d){
    }
    function mouseout(d){
    }
    // Animating the pie-slice requiring a custom function which specifies
    // how the intermediate paths should be drawn.
    function arcTweenSplit(a) {
        var i = d3.interpolate(this._current, a);
        var r = d3.interpolate(prevRadii[a.data.cat], curRadii[a.data.cat]);
        this._current = i(0);
        return function(t) { return d3.arc().innerRadius(0).outerRadius(r(t))(i(t)); };
    }
    function arcTweenFused(a) {
      var i = d3.interpolate(this._current, a);
      var r = d3.interpolate(prevRadii[a.data.cat], curRadii[a.data.cat]);
      this._current = i(0);
      return function(t) { return d3.arc().innerRadius(a.data.cat==0 ? r(t) : fusedMiddleRadius)
                                          .outerRadius(a.data.cat==1 ? r(t) : fusedMiddleRadius)(i(t)); };
    }
    return pC;
}







// TODO: Not used anymore
function flatten_data(data)
{
  var res = {};
  for(var g in data) {
    var sub_group = data[g];
    for(var k in sub_group) {
      res[k] = sub_group[k];
    }
  }
  return res;
}

// TODO: Not used anymore
function stack_groups(data,groups,value_type)
{
  var stacked_data = [];
  var all_grouped_types = [];
  groups.forEach(function (g) {
    if(g.name) {
      
      all_grouped_types = all_grouped_types.concat(g.types);

      stacked_data = stacked_data.concat(
        data.filter(function(d) {return g.types.includes(d.type);}).reduce(function(r, e) {
          r[e.type] = e[value_type];
          r.total += e[value_type];
        // }
          return r;
        }, {group_name:g.name,total:0})
      );
    }
  });
  // insert remaining singletons
  stacked_data = stacked_data.concat(
    data.filter(function(d){ return !all_grouped_types.includes(d.type); })
        .map   (function(d){
          all_grouped_types = all_grouped_types.concat([d.type]);
          var r={group_name:d.type, total:d[value_type]}; r[d.type]=d[value_type]; return r; })
  );

  // use Set to make a unique list of series keys:
  stacked_data.keys = [...new Set(all_grouped_types)];

  return stacked_data;
}

