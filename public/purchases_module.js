


function init_purchases(div_id,in_data1,update_callback) {

  var g_purchases = [];
  var data1 = in_data1;

  // year / type liste déroulante / variante liste déroulante / quantité / kgCO2
  document.getElementById(div_id).innerHTML = `
    <p>TODO: ajouter un slider pour simuler un taux de renouvellement plus lent.</p>
    <button class="collapsible">Liste des achats</button> 
    <div class="content">
    <p class="whatif">
      Durée d&#39amortissement : <input type="number" step="1" min="1" max="20" id="input_purchase_lifetime"> années.
      <span class="note">(il s&#39agit uniquement d&#39un paramètre de lissage temporel, cette valeur n&#39est pas utilisé en tant que durée de vie)</span>
    </p>
    <div style="position:relative;" name="purchases_table_block">
      <input type="button" value="+" name="purchases_add_row" style="position: absolute;bottom: 2px;">
      <table name="purchases_table" style="margin-left:2em;">
        <tr> <th class="transparent"></th> <th>année</th> <th>modèle</th> <th>quantité</th> <th class="transparent"></th> <th>kgCO2e<br>/unité</th></tr>
      </table>
    </div>
    <p></p>
    </div>
    <p></p>
  `;

  var purchases_table = document.querySelector('#'+div_id + ' table[name="purchases_table"]');

  var res = {};

  res.sync = function(new_data1) {
    if(new_data1) {
      data1 = new_data1;

      // rebuild the interface from scratch
      purchases_table.innerHTML = `
        <tr> <th class="transparent"></th> <th>année</th> <th>modèle</th> <th>quantité</th> <th class="transparent"></th> <th>kgCO2e<br>/unité</th></tr>
        `;
      g_purchases = [];
      
      add_all_rows(data1);
      
    }

    for(var year in data1){
      data1[year].purchases = {};
    }
    g_purchases.forEach(function (item) {
      var model = item.model.style.display != "none" && item.model.value!="" ? item.model.value :  undefined;
      //console.log(item.type.value + " , " + item.model.value)
      var factor = get_device_factor(item.type.value,model);
      item.co2span.textContent = factor;
      
      var key = make_device_name(item.type.value,item.model.value);
      var year = item.year.value;
      var dy = data1[year];
      if(!dy) {
        dy = data1[year] = {'purchases':{}};
      }
      dy.purchases = keyval_append(dy.purchases, key, +item.nb.value);
    });
  }

  function add_all_rows(a_data) {
    var data = clone_obj(a_data);
    for(var year in data){
      var dy = data[year].purchases;
      for(var key in dy) {
        add_row(year, key, dy[key]);
      }
    }
  }

  // Function to add a row in the purchase table
  function add_row(year,key,nb)
  {
    var tr_el     = document.createElement('tr');
    var td_year   = document.createElement('td');
    var td_type   = document.createElement('td');
    var td_model  = document.createElement('td');
    var td_nb     = document.createElement('td');
    var td_co2    = document.createElement('td');

    var item_model = device_name_to_type_model(key);
    var item = item_model[0];
    var model = item_model.length==2 ? item_model[1] : "default";

    // console.log("ADD " + item_model + " -> " + item + " , " + model)

    var device_type_el = device_selector_el.cloneNode('deep');
    device_type_el.value = item;
    td_type.appendChild(device_type_el);

    var year_el = year_selector_el.cloneNode('deep');
    year_el.value = year;
    td_year.appendChild(year_el);

    var nb_el = document.createElement('input');
    nb_el.type='number';
    nb_el.step='1';
    nb_el.min='0';
    nb_el.max='99';
    nb_el.value = nb;
    td_nb.appendChild(nb_el);

    var co2_el = document.createElement('span');
    co2_el.className = 'unit';
    td_co2.appendChild(co2_el);

    var device_model_el = document.createElement('select');

    var update_model = function() {
      device_model_el.innerHTML= '';
      if(devices[device_type_el.value] && devices[device_type_el.value].models) {
        for(var m in devices[device_type_el.value].models) {
          var opt_el = document.createElement('option');
          opt_el.value = m;
          opt_el.innerHTML = tr(m);
          device_model_el.appendChild(opt_el);
        }
        device_model_el.value = model;
        device_model_el.style.display = "initial";
      } else {
        device_model_el.style.display = "none";
      }
    }

    // TODO merge this piece of code with init_device
    device_type_el.addEventListener('change', function(){
      update_model();
      res.sync();
      update_callback();
    });
    td_model.appendChild(device_model_el);


    device_model_el.addEventListener("change", function() {
      res.sync();
      update_callback();
    });

    nb_el.addEventListener("change", function() {
      res.sync();
      update_callback();
    });

    year_el.addEventListener("change", function() {
      res.sync();
      update_callback();
    });
    
    // record the different inputs for future uses/deletion
    g_purchases.push({
      year:  year_el,
      type:  device_type_el,
      model: device_model_el,
      nb: nb_el,
      co2span: co2_el
    });

    tr_el.appendChild(td_year);
    tr_el.appendChild(td_type);
    tr_el.appendChild(td_model);
    tr_el.appendChild(td_nb);
    tr_el.appendChild(td_sep.cloneNode("deep")); // TODO add a delete button within this cell
    tr_el.appendChild(td_co2);
    purchases_table.appendChild(tr_el);

    update_model();
  }

  // Create a reference "device name" selector object to be cloned on demand
  var device_selector_el = document.createElement('select');
  purchase_types.forEach(function(item) {
    var opt_el = document.createElement('option');
    opt_el.value = item;
    opt_el.innerHTML = tr(item);
    device_selector_el.appendChild(opt_el);
  });

  var year_selector_el = document.createElement('select');
  for(var y=2010;y<2030;++y) {
    var opt_el = document.createElement('option');
    opt_el.value = y;
    opt_el.innerHTML = y;
    year_selector_el.appendChild(opt_el);
  }

  add_all_rows(data1);

  res.sync();

  document.querySelector('#'+div_id + ' input[name="purchases_add_row"]').addEventListener('click',function(){
    add_row('2019','desktop',1);
    var content = document.getElementById(div_id);
    content.querySelector("div.content").style.maxHeight = content.scrollHeight + "px";
    content.style.maxHeight = content.scrollHeight + "px";
  });

  return res;
}
