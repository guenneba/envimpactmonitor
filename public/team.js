


function add_team_pro_travels_rows(table_id,update_callback) {
  return add_rows(document.getElementById(table_id),z => g_assembled_team_data,undefined,update_callback,
    [
      {group:'travels',     id:'flight_loc',  min:0, max:9999999, step:100, unit:'km', label:'Vols "locaux" :', note:'(destinations atteignables en train de manière raisonable)'},
      {group:'travels',     id:'flight',      min:0, max:9999999, step:1090, unit:'km', label:'Autres vols :', note:''},
      {group:'travels',     id:'tgv',         min:0, max:9999999, step:1090, unit:'km', label:'Train :', note:'(tgv)'},
      // {group:'travels',     id:'bus',         min:0, max:9999999, step:1000, unit:'km', label:'Bus :', note:''},
      {group:'travels',     id:'car',         min:0, max:9999999, step:1000, unit:'km', label:'Voiture :', note:''},
    ]);
}

function init_team_interface(team_data) {

  document.getElementById('team_ph').innerHTML = `

<div id='year_selector_team'  class='year_selector'></div>

<div id='yearly_team'></div>
<!-- <label>
  Normaliser par la taille de l&#39;équipe (empirique) : <input type="checkbox" id="normalization_selector" >
</label> -->

<div class="tab">
  <button class="tablinks tabactive" onclick="open_tab(event, 'team_gen', 'tabcontent2')">Généralités</button>
  <button class="tablinks " onclick="open_tab(event, 'team_travel', 'tabcontent2')">Missions</button>
  <!-- <button class="tablinks " onclick="open_tab(event, 'team_devices', 'tabcontent2')">Équipement bureau</button> -->
  <button class="tablinks " onclick="open_tab(event, 'team_purchases_ph', 'tabcontent2')">Achats</button>
  <button class="tablinks " onclick="open_tab(event, 'team_meals', 'tabcontent2')">Alimentation</button>
  <button class="tablinks " onclick="open_tab(event, 'team_office_way', 'tabcontent2')">Domicile-travail</button>
  <button class="tablinks " onclick="open_tab(event, 'team_clusters_ph', 'tabcontent2')">Clusters</button>
  <button class="tablinks " onclick="open_tab(event, 'team_notes', 'tabcontent2')" style="float:right;">Notes</button>
</div>



<!-- <button class="collapsible">Généralités</button> 
<div class="content"> -->
<div id="team_gen" class="tabcontent2" style="display:block;">
  <p></p>
  <table>
    <tr><td>Nb places "réservées" :</td>
        <td><input type="number" step="1" min="1" max="99" id="team_nb_seats"> </td>
        <td class="note">compter 2 pour les bureaux (même si seul dans le bureau), 12 pour les openspaces,<br> et compter les salles expérimentales au prorata de la superficie d&#39;un bureau</td></tr>
    <tr><td>Nb hommes.années :</td>
        <td><input type="number" step="0.5" min="1" max="99" id="team_nb_men_years"></td>
        <td class="note"></td></tr>
</table>

<p>
<label>Bâtiment, profile : </label><select id="team_building"></select>
</p>
<table>
    <tr><td>Batiment, conso élec :</td>
        <td><input type="number" step="1000" min="0" max="99999999" id="input_team_bat_elec"> <span class="unit">kWh</span></td>
        <td class="note">hors clusters et équipements individuels</td></tr>
    <tr><td>Batiment, conso gaz :</td>
        <td><input type="number" step="1000" min="0" max="99999999" id="input_team_bat_gaz"> <span class="unit">kWh</span></td>
        <td class="note"></td></tr>
    <tr><td>Batiment, réseau de chaleur :</td>
        <td><input type="number" step="1000" min="0" max="99999999" id="input_team_bat_heatnet"> <span class="unit">kWh</span></td>
        <td class="note">facteur : <input type="number" step="0.01" min="0.02" max="0.4" id="input_team_heatnet_factor"> <span class="unit">kgCO2e/kWh</span></td></tr>
    <tr><td>Batiment, capacité :</td>
        <td><input type="number" step="1" min="1" max="9999" id="input_team_bat_seats"> <span class="unit">places</span></td>
        <td class="note">nombre de places théorique</td></tr>
    <tr><td>Batiment, taille :</td>
        <td><input type="number" step="1" min="1" max="9999" id="input_team_bat_workers"> <span class="unit">agents</span></td>
        <td class="note">nombre d&#39;agents effectivement dans le batiment</td></tr>


  </table>
  <p></p>
</div>



<!-- <button class="collapsible">Missions</button>
<div class="content"> -->
<div id="team_travel" class="tabcontent2">
  <p></p>
  <table id="team_mission_table">
    <tr><th class="transparent"></th><th class="transparent"></th><th>actuel</th><!-- <th class="transparent"></th><th>objectif</th> --><th class="transparent"></th><th class="transparent"></th></tr>
  </table>
<p class="whatif">
  Si le train avait été utilisé à la place de l&#39;avion pour les vols locaux, l&#39;équipe aurai économisée <span class="value" id="ph_team_local_flight_gain"></span> kg.CO2 pour l&#39;année,
  soit <span class="value" id="ph_team_local_flight_COP21"></span> % de la cible COP21.
  <!--What if we replace local flights by train? We would save <span class="value" id="ph_local_flight_gain"></span> kg of CO2 every year,
which represents <span class="value" id="ph_local_flight_COP21"></span> % of COP21 target.-->
</p>
<p>
<label>Charger les trajets détaillés à partir d&#39;un fichier CSV : </label><input type="file" id="team_travel_csv_file">
</p>
<div id="team_travel_details_ph"></div>
</div>

<!-- 
<button class="collapsible">Équipement bureau</button>
<div class="content" id="team_devices_ph">
</div>
 -->

<!-- <button class="collapsible">Achats</button>
<div class="content" id="team_purchases_ph"> -->
<div id="team_purchases_ph" class="tabcontent2">
</div>

<!-- <button class="collapsible">Alimentation</button>
<div class="content" id="team_meals_ph"> -->
<div id="team_meals" class="tabcontent2">
<p>Répartition des végétariens et non-végétariens dans l&#39;équipe :</p>
<div class="rangeslidecontainer">
  <input type="range" min="0" max="100" value="10" class="rangeslider2" id="team_meal_percent">
</div>
<div style="float:right;">[<span id="team_nb_vege"></span>] végé</div>
<div style="position:relative;top:0:right:0;">carné [<span id="team_nb_notvege"></span>]</div>
<p></p>
<button class="collapsible">détails</button> 
<div class="content">
  <p class="whatif"><span class="note">Initiallement, commencez par positionner le curseur de manière à reproduire la quantité de kg CO2e issue du questionnaire.</span></p>
  <p class="whatif">Pour info, "carné" et "végé" correspondent aux profils suivants (nombre de repas type par semaine) :
  <table>
  <tr><th></th><th>Boeuf</th><th>Volaille, porc, poisson</th><th>Végétarien</th><th>Végan</th></tr>
  <tr><td>Carné</td><td>5</td><td>7</td><td>2</td><td>0</td></tr>
  <tr><td>Végé</td> <td>0</td><td>0</td><td>8</td><td>6</td></tr>
  </table>
  </p>
</div>

</div>


<div id="team_office_way" class="tabcontent2">
<p>
  Par défaut, les trajets indiqués ont été générés automatiquement à partir des stats du campus de Bordeaux, ce qui donne un ordre de grandeur plausible.</p>
<p class="whatif">Au cours d&#39;une séance de discussion collective du bilan CO2 de votre équipe, faite un tour de table en renseignant chaque membre son trajet type,
  et un mode alternatif qu&#39;il-elle serait prêt-e à tester (pour aller plus vite vous pouvez ignorer les trajets déjà réalisés à pieds ou à vélo).</p>
<p>Nombre de jours par semaine où l&#39;ensemble de l&#39;équipe vient au travail via le mode alternatif choisi :
<span class="value" id="team_officeway_nb_days_span"></span></p>
<div class="rangeslidecontainer">
  <input type="range" min="0" max="5" value="0" class="rangeslider2" id="team_officeway_nb_days_slider">
</div>
<p></p>
<button class="collapsible">détails</button> 
<div class="content">
  <p></p>
  <div id="team_office_way_ph"></div>
  <p></p>
</div>

<p>
<label>Charger les trajets détaillés à partir d&#39;un fichier CSV : </label><input type="file" id="team_officeway_csv_file">
</p>

</div>


<!-- <button class="collapsible">Clusters</button>
<div class="content" id="team_clusters_ph"> -->
<div id="team_clusters_ph" class="tabcontent2">
  <!-- <p></p>
  <table>
    <tr><td>Plafrim :</td>
        <td><input type="number" step="0.1" min="0" max="100" id="input_team_plafrim_percentage"> <span class="unit">%</span></td>
        <td class="note">pourcentage d&#39;utilisation de la plateforme par an</td></tr>

    <tr><td>Autres clusters :</td>
        <td><input type="number" step="10" min="0" max="999999" id="input_team_cluster_cpu_hours"> <span class="unit">heures</span></td>
        <td class="note">
          consommation totale d&#39;un CPU: <input type="number" step="10" min="0" max="9999" id="input_team_cluster_conso">W &nbsp;
          PUE du cluster: <input type="number" step="0.1" min="1.2" max="10" id="input_team_cluster_pue">
        </td></tr>
  </table>
  <p></p> -->
</div>


<!-- <button class="collapsible">Notes</button>
<div class="content"> -->
<div id="team_notes" class="tabcontent2">
  <p>
  <ul>
    <li>Missions, ajouter l&#39;effet des contrails</li>
    <li>Missions, ajouter la possibiliter de charger une liste de trajets individualiser pour 1) proposer une classification automatique des vols remplaçables (selon différents critères), et 2) afficher les répartitions entre les agents.</li>
    <li>Repas, afficher le détails des deux régimes proposés</li>
    <li>Batiment, ajouter des profiles simpliés pour celles et ceux ne connaissant pas les consommations réels</li>
    <li>Batiment, ajouter des conversions auto entre places et m^2</li>
    <li>Trajets domicile-travail, ajouter différents profiles, et un profil avancé permettant de renseigner toutes les info précisément collectées</li>
    <li>Matériel IT, ajouter une vue de l&#39;évolution au fils des années</li>
    <li>Clusters, ajouter les info des autres clusters type.</li>
    <li>Clusters, ajouter une estimation de la durée de vie effective</li>
    <li></li>
  </ul>
  </p>
  <div class="warning">
    <p>
      Attention, cela ne permet pas d'évaluer le bilan carbone des recherches menées par l'équipe, il manque notamment:
      <ul>
        <li>la part du bilan des équipes supports (RH, SAF, assistante, etc)</li>
        <li>la part du bilan des agences de financements (ANR, europe, etc.)</li>
        <li>la part du bilan des supports de nos publications (conférence, revues, vidéos, etc.)</li>
        <li>les missions non prises par Oreli</li>
        <li>etc.</li>
      </ul>
    </p>
  </div>
</div>
  `;

  // TODO remove it
	function extract_yearly_data(selected_year) {
	  if(!selected_year) selected_year = g_year;
	  var res = team_data[selected_year];
    res['building'] = buildings[team_data.default.building];
    return res;
	}

	function update_myself() {
      allY_BP.update(g_value_type);
      g_assembled_team_data = assemble_team_data(g_year);
      yearly_team_BP.update(g_year,g_value_type);
      update_local_flight( g_year);
    }

	// compute and display gains if local flights are replaced by train
  function update_local_flight(selected_year) {
    yData = extract_yearly_data(selected_year);
    flight_CO2 = yData.travels['flight_loc'] * conv.to_CO2.flight_loc;
    train_CO2 =  yData.travels['tgv']      * conv.to_CO2.tgv;
    gain_CO2 = flight_CO2-train_CO2;
    document.getElementById('ph_team_local_flight_gain').textContent = toFixed(gain_CO2,2);
    document.getElementById('ph_team_local_flight_COP21').textContent = toFixed(100*Math.abs(gain_CO2)/params.constants.COP21_CO2,2);
  }

  function purchases2devices(data,selected_year) {
    var res = {
      'desktop_CO2' : 0,
      'screen_CO2' : 0,
      'laptop_CO2' : 0,
    };

    var nb_years = 0;
    for(var year in data){
      var ydata = data[year];
      var dist = Number(selected_year)-Number(year);
      if(dist>=0 && dist<params.purchase_lifetime) {
        nb_years++;
        var dy = ydata.purchases;
        for(var k in dy) {

          var type_model = device_name_to_type_model(k);
          var type = type_model[0];
          var model = undefined;
          if(type_model.length==2)
            model = type_model[1];

          var output_type = {
            'gpu': 'desktop',
            'videoprojector': 'screen'
          }[type];
          if(!output_type)
            output_type = type;

          var type_co2 = output_type+'_CO2';

          if(!(type_co2 in res))
            res[type_co2] = 0;

          var factor = get_device_factor(type,model);

          res[type_co2] += Number(dy[k]) * factor / params.purchase_lifetime;;
        }
      }

    }
    return res;
  }

	function assemble_team_data(selected_year) {

    yData = extract_yearly_data(selected_year);

    var building_usage_factor = yData.seats / yData.building.seats; 
    var building_elec = building_usage_factor * yData.building.elec;

    var num_meals = 0;
    for(var k in g_data.user.meals) { num_meals += g_data.user.meals[k]; }

    var percent_vege = document.getElementById("team_meal_percent").value/100;

    var team_meals = clone(meal_profiles['carné'],params.working_days/14*yData.workers*(1-percent_vege));
    var vege_meals = clone(meal_profiles['végé'], params.working_days/14*yData.workers*percent_vege);
    for(var k in vege_meals)
      team_meals[k] += vege_meals[k];
    var avg_user = clone(team_meals,14/params.working_days/yData.workers);
    for(var k in avg_user)
      avg_user[k] = toFixed(avg_user[k],1);
    console.log(avg_user);

    function reduce_office_way(data) {
      return data.map(function(el) {
        if(typeof el.val === 'object')
          return {key:el.key,val:el.val.dist*el.val.occperweek/5.0/(el.val.passengers+1)};
        else
          return el;
      });
    }

    var mix_officeways = document.getElementById("team_officeway_nb_days_slider").value/5.0;
    var to_office1 = clone(keyval_toObject(reduce_office_way(yData.to_office)));
    var to_office2 = keyval_toObject(reduce_office_way(g_officeway_objectives));
    for(var k in to_office1) {
      var v2 = to_office2[k] ? to_office2[k] : 0;
      to_office1[k] = (1-mix_officeways) * to_office1[k] + mix_officeways * v2;
    }
    for(var k in to_office2) {
      if(!(k in to_office1)) {
        to_office1[k] = mix_officeways * to_office2[k];
      }
    }

    var res = {
      travels: yData.travels,

      to_office: clone(to_office1, params.working_days), //clone(center_default.to_office, params.working_days*yData.workers),
      devices: purchases2devices(team_data,selected_year),
      meals: team_meals,
      energy: {
        'env_elec': building_elec,
      },
      clusters: process_cluster_list(yData.clusters),
    };

    if(yData.building.gaz_nat_kWh)
      res.energy['gaz_nat_kWh'] = building_usage_factor * yData.building.gaz_nat_kWh;

    if(yData.building.heatnet)
      res.energy['heatnet'] = building_usage_factor * yData.building.heatnet;

    return res;
  }

  // automatically fill to-office travels by sampling statistical data
  {
    // compute CDF
    var cdf = clone(stats.bdx_to_office.ratio);
    var count = 0.0;
    for(var k in cdf) {
      count += cdf[k];
      cdf[k] = count;
    }
    if(Math.abs(count-1)>1e-6)
      console.log("ERROR, stats.bdx_to_office.ratio must sumup to 1, got : " + count);

    for(var year in team_data){
      var ydata = team_data[year];
      var office_ways = [];
      for(var i=0; i<ydata.workers; ++i) {
        var u = (i+0.5)/ydata.workers;
        var type;
        var prev = 0;
        for(var k in cdf) {
          if(u>prev && u<=cdf[k])
            type = k;
          prev = cdf[k];
        }
        
        office_ways.push({key:type, val:Math.round(stats.bdx_to_office.avg_dist[type])});
      }

      ydata['to_office'] = office_ways;
    }
  }

  var g_officeway_objectives = clone_obj(extract_yearly_data(g_year).to_office);
  g_officeway_objectives = g_officeway_objectives.map(function(el){ return {key:undefined,val:undefined} })
  var travel_details_ui;

  g_assembled_team_data = assemble_team_data(g_year);

	var allY_BP = make_all_years_barplot(team_data, g_value_type,'team',
	    {click: function(d) {
	        g_year = d.year;
          update_myself();
          
          var yData = extract_yearly_data(g_year);
          ctrls.forEach(function(el) {el.sync(yData)});
          g_officeway_objectives = clone_obj(yData.to_office);
          officeway_ui.sync(yData.to_office,g_officeway_objectives)
	        update_local_flight(g_year);

          if(travel_details_ui)
            travel_details_ui.sync();
	        
          team_mission_table.update();
	    }});

	var yearly_team_PC    = {};//make_yearly_pie_chart(g_year,g_value_type,'team');
	var yearly_team_BP   = make_yearly_barplot(null, g_year,g_value_type,'team',(y,v) => compute_CO2_and_kWh(g_assembled_team_data),{dir:'horizontal', width: 800});

  var ctrls = [];
	
  var dummy_team_data2 = clone_obj(extract_yearly_data(g_year));
  // ctrls.push(init_devices('team_devices_ph', extract_yearly_data(g_year), dummy_team_data2, update_myself));
  ctrls.push(init_clusters('team_clusters_ph', extract_yearly_data(g_year), update_myself));
  var officeway_ui = init_office_way('team_office_way_ph',  extract_yearly_data(g_year).to_office, g_officeway_objectives, update_myself);

  var building_ctrls = [];
  building_ctrls.push(init_num_controller('input_team_bat_elec',z => buildings[team_data.default.building],'elec', null, update_myself));
  building_ctrls.push(init_num_controller('input_team_bat_gaz',z => buildings[team_data.default.building],'gaz_nat_kWh', null, update_myself));
  building_ctrls.push(init_num_controller('input_team_bat_heatnet',z => buildings[team_data.default.building],'heatnet', null, update_myself));
  building_ctrls.push(init_num_controller('input_team_heatnet_factor',conv.to_CO2,'heatnet', null, update_myself));
  building_ctrls.push(init_num_controller('input_team_bat_seats',z => buildings[team_data.default.building],'seats', null, update_myself));
  building_ctrls.push(init_num_controller('input_team_bat_workers',z => buildings[team_data.default.building],'workers', null, update_myself));

  {
    var building_selector = document.getElementById("team_building");
    for(var k in buildings) {
      var opt_el = document.createElement('option');
      opt_el.value = k;
      opt_el.innerHTML = buildings[k].name;
      building_selector.appendChild(opt_el);
    }
    building_selector.value = team_data.default.building;
    building_selector.addEventListener("change", function() {
      team_data.default.building = this.value;

      building_ctrls.forEach(function(el) {el.sync()});
      
      update_myself();
    });
  }

  {
    var slider = document.getElementById("team_meal_percent");
    var nb_vege_el = document.getElementById("team_nb_vege");
    var nb_notvege_el = document.getElementById("team_nb_notvege");
    slider.oninput = function() {
      var nb_workers = extract_yearly_data(g_year).workers;
      nb_vege_el.innerHTML    = toFixed(     this.value *nb_workers/100,1);
      nb_notvege_el.innerHTML = toFixed((100-this.value)*nb_workers/100,1);
      update_myself();
    }
    slider.oninput();
  }

  {
    var slider = document.getElementById("team_officeway_nb_days_slider");
    var nb_el = document.getElementById("team_officeway_nb_days_span");
    slider.oninput = function() {
      nb_el.textContent = this.value;
      update_myself();
    }
    slider.oninput();
  }

  {
    function handle_team_travel_csv_file_select(files) {
    if(files.length==1) {
      var file = files[0];
      var reader = new FileReader();

      reader.onload = (function(theFile) {
          return function(e) {
            var csv_text = e.target.result;

            var travel_data = d3.csvParse(csv_text,function(d) {
              return d;
            });

            // console.log(travel_data)

            // clear travels
            for(var y in team_data) {
              var dy = team_data[y];
              if(dy.travels) {
                ['flight', 'flight_loc', 'tgv'].forEach(function(k) {
                  if(k in dy.travels) {
                    dy.travels[k] = 0;
                  }
                });
              }
            }

            // recompute travels
            travel_data.forEach(function(el) {
              if((+el.dist)>0) {
                var dy = team_data[el.year];
                if(dy && dy.travels) {
                  dy = dy.travels;
                  var mode = el.mode == 'train' ? 'tgv'
                           : el.type == 'DOMESTIC' ? 'flight_loc' : 'flight' ;
                  if(mode in dy)
                    dy[mode] += +el.dist;
                  else
                    dy[mode] = +el.dist;
                }
              }
            });

            // console.log(team_data[g_year]);
            // g_assembled_team_data = assemble_team_data(g_year);
            // console.log(g_assembled_team_data);
            team_mission_table.update();
            update_myself();

            travel_details_ui = init_travel_details('team_travel_details_ph', travel_data, el => el.year==g_year );
          };
        })(file);

        reader.readAsText(file);
      }
    }

    document.getElementById('team_travel_csv_file').addEventListener('change', function(evt) { return handle_team_travel_csv_file_select(evt.target.files); }, false);
  }

  {
    function handle_team_officeway_csv_file_select(files) {
      if(files.length==1) {
        var file = files[0];
        var reader = new FileReader();

        reader.onload = (function(theFile) {
          return function(e) {
            var csv_text = e.target.result;

            var to_office_data = d3.csvParse(csv_text,function(d) {
              return d;
            });

            var ydata = team_data[g_year];

            keyval_clear(ydata.to_office);
            keyval_clear(g_officeway_objectives);

            // copy office ways
            to_office_data.forEach(function(el) {
              if((+el.dist)>0) {
                var occperweek = 5;
                if(Number(el.occperweek))
                  occperweek = Number(el.occperweek);
                ydata.to_office = keyval_append(ydata.to_office,el.way,{dist:Number(el.dist), passengers:filter_nan(Number(el.passengers)), occperweek:occperweek});
                g_officeway_objectives = keyval_append(g_officeway_objectives,
                  el.alt_way ? el.alt_way : el.way,
                  { dist:Number(el.alt_dist ? el.alt_dist : el.dist),
                    passengers:filter_nan(Number(el.alt_passengers)),
                    occperweek:occperweek});
              }
            });

            // console.log(clone(ydata.to_office));
            // console.log(clone(g_officeway_objectives));
            officeway_ui.sync(ydata.to_office,g_officeway_objectives);
            update_myself();
          };
        })(file);

        reader.readAsText(file);
      }
    }

    document.getElementById('team_officeway_csv_file').addEventListener('change', function(evt) { return handle_team_officeway_csv_file_select(evt.target.files); }, false);
  }

  init_purchases('team_purchases_ph', team_data, update_myself)

  var team_mission_table = add_team_pro_travels_rows('team_mission_table', update_myself);

  ctrls.push(init_num_controller('team_nb_seats',extract_yearly_data(g_year),'seats', null, update_myself));
  ctrls.push(init_num_controller('team_nb_men_years',extract_yearly_data(g_year),'workers', null, update_myself));

  init_num_controller('input_purchase_lifetime',params,'purchase_lifetime', null, update_myself);

	update_local_flight( g_year);
  update_myself();
  make_collapsible();
}

var g_assembled_team_data;
