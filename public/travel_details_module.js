
function init_travel_details(div_id,in_data1,_filter,update_callback) {

  var g_travels = [];
  var data1 = in_data1;

  var filter;

  // year / type liste déroulante / variante liste déroulante / quantité / kgCO2
  document.getElementById(div_id).innerHTML = `
    
    <button class="collapsible">Détails des trajets</button> 
    <div class="content">
    <p></p>
    <div style="position:relative;" name="travel_table_block">
      <!--<input type="button" value="+" name="travel_add_row" style="position: absolute;bottom: 2px;">-->
      <table name="travel_table" style="margin-left:2em;font-size:80%">
      </table>
    </div>
    <p></p>
    </div>
    <p></p>
  `;

  make_collapsible();

  var travel_table = document.querySelector('#'+div_id + ' table[name="travel_table"]');

  var res = {};

  res.sync = function(new_data1,_filter) {

    if(new_data1) {
      data1 = new_data1;
    }

    if( (!_filter) && (!filter) ) {
      filter = function(x) {return true;};
    } else if(_filter) {
      filter = _filter;
    }

    // rebuild the interface from scratch
    travel_table.innerHTML = `
      <tr> 
      <th></th>  <th>origine</th> <th>destination</th> <th>mode</th> <th>type</th> <th>km</th> <th>kgCO2e</th>
      </tr>
      `;

    g_travels = [];
    
    add_all_rows(data1);

  }

  function add_all_rows(a_data) {
    // cluster data by mission id
    var group_list = {};
    var keys = [];
    a_data.forEach(function (el) {
      if(filter(el)) {
        var key = el['mission id'] + "," + el.user;
        if(key in group_list) {
          group_list[key].push(el);
        } else {
          group_list[key] = [el];
          keys.push(key);
        }
      }
    });
    keys.forEach(function(key) {
      var group = group_list[key];
      add_group(group[0])
      group.forEach(function (el) {
        add_row(el);
      });
    });
    // data.forEach(function (el) {
    //   if(filter(el))
    //     add_row(el);
    // });

    var content = document.getElementById(div_id).querySelector("div.content");
    if(content.style.maxHeight)
      content.style.maxHeight = content.scrollHeight + "px";
  }

  function add_group(item) {
    var tr_el = document.createElement('tr');
    tr_el.className = "travel_detail_group_row"
    
    append_td(tr_el).innerHTML = "" + item.year;
    var td = append_td(tr_el);
    td.colSpan = 6;
    td.innerHTML = item.label + " -- " + item.user + " / " + item['mission id'];
    // append_td(tr_el).innerHTML = item['mission id'];
    // td = append_td(tr_el);
    // td.innerHTML = item.user + " / " + item['mission id'];
    // td.colSpan = 2;
    travel_table.appendChild(tr_el);
  }

  // Function to add a row in the purchase table
  function add_row(item)
  {
    // console.log("add row ");
    var tr_el = document.createElement('tr');

    tr_el.appendChild(td_sep.cloneNode("deep"));
    // append_td(tr_el).innerHTML = "" + item.year;
    // append_td(tr_el).innerHTML = item['mission id'];
    // append_td(tr_el).innerHTML = item.user;
    append_td(tr_el).innerHTML = item.cityA;
    append_td(tr_el).innerHTML = item.cityB;
    append_td(tr_el).innerHTML = item.mode;
    append_td(tr_el).innerHTML = item.type;
    append_td(tr_el).innerHTML = toFixed(item.dist,0);
    append_td(tr_el).innerHTML = toFixed(item.co2e,1);

    // var label_el = document.createElement('span');
    // label_el.className = 'note';
    // label_el.textContent = item.label;
    // append_td(tr_el,label_el);

    travel_table.appendChild(tr_el);

    // trigger_onchange(device_type_el);
  }

  res.sync(in_data1,_filter);

  // document.querySelector('#'+div_id + ' input[name="purchases_add_row"]').addEventListener('click',function(){
  //   add_row('2019','desktop',1);
  //   var content = document.getElementById(div_id);
  //   content.querySelector("div.content").style.maxHeight = content.scrollHeight + "px";
  //   content.style.maxHeight = content.scrollHeight + "px";
  // });

  return res;
}
