

function clone(x,scale,prefix) {
  if(!scale)  scale = 1;
  if(!prefix) prefix = '';
  var res = {};
  for(k in x) {
    res[prefix+k] = x[k] * scale;
  }
  return res;
}

function clone_obj(x) {
   var res;
   res = Array.isArray(x) ? [] : {};
   for (key in x) {
       var v = x[key];
       res[key] = (typeof v === "object") ? clone_obj(v) : v;
   }
   return res;
}

function toFixed(value, precision) {
  var power = Math.pow(10, precision || 0);
  return Math.round(value * power) / power;
}


function keyval_foreach(d,f) {
  if(Array.isArray(d)) {
    d.forEach(e => f(e.key, e.val));
  } else {
    for(var k in d) {
      f(k, d[k]);
    }
  }
}

function keyval_get(d, k) {
  if(Array.isArray(d)) {
    var el = d.find(e => e.key==k);
    if(el)
      return el.val;
    else
      return undefined;
  } else {
    return d[k];
  }
}

function keyval_append(d,k,v) {
  if(Array.isArray(d)) {
    d.push({key:k,val:v});
  } else {
    if(!d[k])
      d[k] = v;
    else
      d[k] += v;
  }
  return d;
}

function keyval_toObject(d) {
  if(Array.isArray(d)) {
    res = {};
    keyval_foreach(d,function(k,v){
      if(!res[k])
        res[k] = v;
      else
        res[k] += v;
    });
    return res;
  } else {
    return d;
  }
}

function keyval_sumall(d) {
  var res = 0;
  keyval_foreach(d,function(k,v){
    res += v;
  });
  return res;
}

function keyval_clear(d) {
  if(Array.isArray(d)) {
    d.length = 0;
  } else {
    for(var k in d) delete d[k];
  }
}

